package com.muxe.partyinvite

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class AppClass : Application()