package com.muxe.partyinvite.ui.ticketInfo

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Tickets
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityTicketInfoBinding
import com.muxe.partyinvite.ui.confirmTickets.ConfirmTicketsActivity
import com.muxe.partyinvite.ui.main.MainActivity
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class TicketInfoActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityTicketInfoBinding
    private lateinit var ticket : Tickets
    private val viewModel: TicketInfoViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTicketInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.layProceed.setOnClickListener(this)
        //init dialog
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        try {
            val idParty = intent.extras?.getInt(ConfirmTicketsActivity.DATA)!!
            viewModel.getTicketById(idParty)
            populateData()
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateUi() {
        binding.tvTitle.text = ticket.party?.label
        binding.tvPlace.text = "${ticket.party?.address}, ${ticket.party?.postalcode}, ${ticket.party?.city}"
        binding.tvDate.text = ValueFormat.formatDateTime(ticket.date)
        binding.tvTime.text = ValueFormat.formatTime(ticket.date)
        binding.tvVisitor.text = "${ticket.user?.firstname} ${ticket.user?.lastname}"
        ticket.party?.cover?.url?.let { showImageCenterCrop(it, binding.imgContent) }
        ticket.qr?.let { showImageCenterInside("https://party.qinox.nl$it", binding.imgBarcode) }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.layProceed -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun populateData() {
        viewModel.ticketResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        ticket = it
                        updateUi()
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }
}