package com.muxe.partyinvite.ui.accountInfo

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.databinding.ItemEventHorizontalBinding
import com.muxe.partyinvite.utils.ValueFormat
import com.muxe.partyinvite.utils.showImageCenterInside

class EventHorizontalAdapter(private val context: Context, private val partyListItem: ArrayList<Party>,
                    private val clickListener: (Party) -> Unit, private val clickFav: (Int?) -> Unit)
    : RecyclerView.Adapter<EventHorizontalAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemEventHorizontalBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return partyListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(partyListItem[position])
    }

    inner class ViewHolder(private val binding: ItemEventHorizontalBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: Party) {
            binding.tvTitle.text = data.label
            binding.tvPlace.text = "${data.address}, ${data.postalcode}, ${data.city}"
            binding.tvDistance.text = "${data.distance} km afstand"
            binding.tvDate.text = ValueFormat.formatDateTime(data.date)
            binding.tvPrice.text = "€ ${data.price}"
            data.cover?.url?.let { context.showImageCenterInside(it, binding.imgContent) }
            binding.tvFav.text = data.favoritedCount.toString()
            if (data.isFavorite == true) {
                binding.tvFav.setTextColor(ContextCompat.getColor(context, R.color.black))
                binding.imgFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_24))
            } else {
                binding.tvFav.setTextColor(ContextCompat.getColor(context, R.color.white))
                binding.imgFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_border_24))
            }
            itemView.setOnClickListener{
                clickListener(data)
            }

            binding.imgFav.setOnClickListener{
                clickFav(data.id)
            }
        }
    }

    fun updateData(newList: List<Party>) {
        partyListItem.clear()
        partyListItem.addAll(newList)
        notifyDataSetChanged()
    }
}