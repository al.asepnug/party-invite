package com.muxe.partyinvite.ui.participantsList

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityParticipantsListBinding
import com.muxe.partyinvite.ui.followersDetail.FollowersDetailActivity
import com.muxe.partyinvite.utils.makeLoading
import com.muxe.partyinvite.utils.makeSuccess
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class ParticipantsListActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityParticipantsListBinding
    private val listParticipantsItem = ArrayList<Owner>()
    private lateinit var listParticipants : ArrayList<Owner>
    private val viewModel: ParticipantsViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private var idParty: Int? = null
    private var idUser: Int? = null
    private var typeView: String? = null

    companion object {
        const val DATA = "data_participants"
        const val ID_PARTY = "id_party"
        const val ID_USER = "id_user"
        const val TYPE_VIEW = "type_view"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityParticipantsListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        try {
            if (intent.extras?.containsKey(DATA) == true) {
                listParticipants = intent.extras?.getParcelableArrayList(DATA)!!
                if (intent.extras?.containsKey(ID_PARTY) == true) {
                    idParty = intent.extras?.getInt(ID_PARTY)
                    supportActionBar?.title = "Alle Gesten"
                } else if (intent.extras?.containsKey(ID_USER) == true){
                    idUser = intent.extras?.getInt(ID_USER)
                    if (intent.extras?.containsKey(TYPE_VIEW) == true) {
                        typeView = intent.extras?.getString(TYPE_VIEW)
                        supportActionBar?.title = typeView
                    }
                }
                initUi()
                populateData()
            }
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when (v!!.id) {

        }
    }

    private fun initUi() {
        binding.rvParticipants.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = ParticipantsAdapter(context, listParticipantsItem, clickListener = {
                val intent = Intent(context, FollowersDetailActivity::class.java)
                intent.putExtra(FollowersDetailActivity.DATA, it)
                context.startActivity(intent)
            }, clickFollowing = {
                if (it.isFollowing == true) {
                    viewModel.deleteFollow(it.id)
                } else {
                    viewModel.postFollow(it.id)
                }
            })
        }

        binding.rvParticipants.adapter?.let { adapter ->
            when (adapter) {
                is ParticipantsAdapter -> {
                    try {
                        adapter.updateData(listParticipants)
                    } catch (e: IOException) {
                        toast("Error Load Data")
                    }
                }
            }
        }
    }

    private fun populateData() {
        viewModel.partyResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvParticipants.adapter?.let { adapter ->
                            when (adapter) {
                                is ParticipantsAdapter -> {
                                    try {
                                        it.participants.let { data ->
                                            if (data != null && data.isNotEmpty()) {
                                                adapter.updateData(data)
                                            } else {
                                                adapter.updateData(listOf(Owner()))
                                            }
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {

                }
            }
        })

        viewModel.messageResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        makeSuccess(title = "Success", content = "Successfully " +
                                "${it.status}", confirmListener = {
                            if (it.status.equals("Followed")) {
                                if (idParty != null) {
                                    viewModel.getPartyById(idParty)
                                }
                                if (idUser != null) {
                                    viewModel.getUser(idUser!!)
                                }
                            } else if (it.status.equals("Unfollowed")){
                                if (idParty != null) {
                                    viewModel.getPartyById(idParty)
                                }
                                if (idUser != null) {
                                    viewModel.getUser(idUser!!)
                                }
                            }
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })

        viewModel.userResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        binding.rvParticipants.adapter?.let { adapter ->
                            when (adapter) {
                                is ParticipantsAdapter -> {
                                    try {
                                        if (typeView.equals("followers")) {
                                            it.followers.let { data ->
                                                if (data != null && data.isNotEmpty()) {
                                                    adapter.updateData(data)
                                                } else {
                                                    adapter.updateData(listOf(Owner()))
                                                }
                                            }
                                        } else if (typeView.equals("following")) {
                                            it.following.let { data ->
                                                if (data != null && data.isNotEmpty()) {
                                                    adapter.updateData(data)
                                                } else {
                                                    adapter.updateData(listOf(Owner()))
                                                }
                                            }
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }
}