package com.muxe.partyinvite.ui.pageEvent.detailsPageEvent

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.ms.square.android.expandabletextview.ExpandableTextView
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.FragmentDetailsPageEventBinding
import com.muxe.partyinvite.ui.allEvent.EventAllFragment
import com.muxe.partyinvite.ui.confirmTickets.ConfirmTicketsActivity
import com.muxe.partyinvite.ui.followersDetail.FollowersDetailActivity
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.ui.pageEvent.PageEventViewModel
import com.muxe.partyinvite.ui.pageEvent.ParticipantAdapter
import com.muxe.partyinvite.ui.participantsList.ParticipantsListActivity
import com.muxe.partyinvite.ui.participantsList.ParticipantsListActivity.Companion.ID_PARTY
import com.muxe.partyinvite.utils.OverlapDecoration
import com.muxe.partyinvite.utils.ValueFormat
import com.muxe.partyinvite.utils.ValueFormat.formatDate
import com.muxe.partyinvite.utils.ValueFormat.formatDateTimed
import com.muxe.partyinvite.utils.showImageRounded
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class DetailsPageEventFragment : Fragment(), OnMapReadyCallback, View.OnClickListener,
    GoogleMap.OnMarkerClickListener {
    private val binding by lazy { FragmentDetailsPageEventBinding.inflate(layoutInflater) }
    private val viewModel: PageEventViewModel by viewModels()
    private var party: Party? = null
    private lateinit var mMap: GoogleMap
    private var markerParty: Marker? = null
    private val listParticipant = ArrayList<Owner>()
    private var participantCount : Int = 0

    companion object {
        const val ARG_NAME = PageEvent.DATA
        fun newInstance(data: Party): DetailsPageEventFragment {
            val fragment = DetailsPageEventFragment()

            val bundle = Bundle().apply {
                putParcelable(ARG_NAME, data)
            }

            fragment.arguments = bundle

            return fragment
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        binding.btnBooking.setOnClickListener(this)
        binding.btnShare.setOnClickListener(this)
        binding.layParticipantCount.setOnClickListener(this)
        initUi()
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //(activity?.supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment?)?.getMapAsync(this)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        if (party != null) {
            binding.tvTitle.text = party?.label
            binding.tvPlace.text = "${party?.address}, ${party?.postalcode}, ${party?.city}"
            binding.tvDistance.text = "${party?.distance} km afstand"
            binding.tvDate.text = ValueFormat.formatDateTime(party?.date)
            binding.tvTime.text = ValueFormat.formatTime(party?.date)
            binding.tvPrice.text = "€ ${party?.price}"
            binding.tvTotalParticipant.text = party?.participantsCount.toString()
            binding.expandedTextView.text = party?.description
//            binding.expandableText.text = party?.description
            party?.cover?.url?.let { requireActivity().showImageRounded(it, binding.imgContent) }
            populateData()
            viewModel.getPartyById(party?.id)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            if (arguments?.containsKey(ARG_NAME) == true) {
                party = arguments?.getParcelable(ARG_NAME)
            } else {
                requireActivity().toast("Error Load Data")
            }
        } catch (e: IOException) {
            requireActivity().toast("Error Load Data")
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = false
        mMap.uiSettings.isScrollGesturesEnabled = false
        try {
            if (party?.latitude != null && party?.longitude != null) {
                val currentLatLng = LatLng(party?.latitude!!, party?.longitude!!)
                markerParty = mMap.addMarker(
                    MarkerOptions().position(currentLatLng).title("Feestlocatie").icon(
                        ValueFormat.bitmapDescriptorFromVector(
                            requireContext(),
                            R.drawable.ic_pin
                        )
                    )
                )
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12.0f))
            }
        } catch (e: IOException) {
            requireActivity().toast("Error Load Data")
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnBooking -> {
                val intent = Intent(context, ConfirmTicketsActivity::class.java)
                intent.putExtra(PageEvent.DATA, party)
                context?.startActivity(intent)
            }

            R.id.btnShare -> {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, "Feest ${party?.label}  aan ${formatDateTimed(party?.date)}" +
                            "in ${party?.address}, ${party?.city}" +
                            "\nklik voor meer ${party?.shareUrl}")
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, "Doe mee met dit feest")
                startActivity(shareIntent)
            }

            R.id.layParticipantCount -> {
                if (participantCount > 0) {
                    val intent = Intent(context, ParticipantsListActivity::class.java)
                    intent.putParcelableArrayListExtra(ParticipantsListActivity.DATA, listParticipant)
                    intent.putExtra(ID_PARTY, party?.id)
                    context?.startActivity(intent)
                }
            }
        }
    }

    override fun onMarkerClick(p0: Marker) = false

    private fun initUi() {
        binding.rvParticipants.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            addItemDecoration(OverlapDecoration())
            adapter = ParticipantAdapter(context, listParticipant) {
                val intent = Intent(context, FollowersDetailActivity::class.java)
                intent.putExtra(FollowersDetailActivity.DATA, it)
                context.startActivity(intent)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun populateData() {
        viewModel.partyResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvParticipants.adapter?.let { adapter ->
                            when (adapter) {
                                is ParticipantAdapter -> {
                                    try {
                                        it.participants.let { data ->
                                            if (data != null && data.isNotEmpty()) {
                                                listParticipant.clear()
                                                listParticipant.addAll(data)
                                                participantCount = data.size
                                                if (data.size > 10) {
                                                    adapter.updateData(data.takeLast(10))
                                                    binding.layParticipantCount.visibility = View.VISIBLE
                                                    binding.tvParticipantCount.text =
                                                        "+${party?.participantsCount?.minus(10)}"
                                                } else {
                                                    adapter.updateData(data)
                                                }
                                            } else {
                                                adapter.updateData(listOf(Owner()))
                                            }
                                        }
                                    } catch (e: IOException) {
                                        requireActivity().toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {

                }
            }
        })
    }
}