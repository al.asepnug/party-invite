package com.muxe.partyinvite.ui.participantsList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ParticipantsViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _messageResponse = MutableLiveData<ApiResponse<Message>>()
    private val _partyResponse = MutableLiveData<ApiResponse<Party>>()
    private val _userResponse = MutableLiveData<ApiResponse<UserDetails>>()

    val messageResponse = _messageResponse
    val partyResponse = _partyResponse
    val userResponse = _userResponse

    fun postFollow(idUser: Int?) {
        viewModelScope.launch {
            dataRepository.postFollow(idUser).collect{
                _messageResponse.value = it
            }
        }
    }

    fun deleteFollow(idUser: Int?) {
        viewModelScope.launch {
            dataRepository.deleteFollow(idUser).collect{
                _messageResponse.value = it
            }
        }
    }

    fun getPartyById(id: Int?) {
        viewModelScope.launch {
            dataRepository.getPartyById(id).collect{
                _partyResponse.value = it
            }
        }
    }

    fun getUser(idUser: Int) {
        viewModelScope.launch {
            dataRepository.getFullProfile(idUser).collect{
                _userResponse.value = it
            }
        }
    }


}