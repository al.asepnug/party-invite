package com.muxe.partyinvite.ui.partyFollowers

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Location
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PartyFollowersViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _partyFollowersResponse = MutableLiveData<ApiResponse<List<Party>>>()
    private val _partyFavoriteResponse = MutableLiveData<ApiResponse<Message>>()

    val partyFollowersResponse = _partyFollowersResponse
    val partyFavoriteResponse = _partyFavoriteResponse

    fun getLocation(): Location = dataRepository.getLocation()

    fun getPartyFollowers(path: String?) {
        viewModelScope.launch {
            dataRepository.getParty(path).collect{
                _partyFollowersResponse.value = it
            }
        }
    }

    fun getPartyFavorite(id: Int?) {
        viewModelScope.launch {
            dataRepository.getPartyFavorite(id).collect{
                _partyFavoriteResponse.value = it
            }
        }
    }

}