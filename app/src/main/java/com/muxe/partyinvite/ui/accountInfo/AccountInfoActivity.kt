package com.muxe.partyinvite.ui.accountInfo

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityAccountInfoBinding
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class AccountInfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAccountInfoBinding
    private val viewModel: AccountInfoViewModel by viewModels()
    private val listParty = ArrayList<Party>()
    private var user: User? = null
    private lateinit var pDialog: SweetAlertDialog
    private var path: String? = null

    companion object {
        const val DATA_USER = "data_user"
        const val PATH_MINE = "mine"
        const val PATH_FAVORITE = "personal"
        const val PATH_JOINED = "joined"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.title = "Account"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        try {
            user = intent.extras?.getParcelable(DATA_USER)
            initUi()
            viewModel.getPartyFollowers("followers")
            viewModel.getPartyMine(PATH_MINE)
            viewModel.getPartyFav(PATH_FAVORITE)
            viewModel.getPartyJoined(PATH_JOINED)
            populateData()
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun initUi() {
        binding.tvName.text = "${user?.firstname} ${user?.lastname}"
        binding.tvEmail.text = user?.email
        user?.avatar?.url?.let { showImageRounded(it, binding.imgProfile) }
        binding.rvFollower.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = PartyFollowerAdapter(context, listParty) {
            }
        }

        binding.rvPartyMine.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = EventHorizontalAdapter(context, listParty, clickListener = {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, it)
                context.startActivity(intent)
            }, clickFav = {
                path = PATH_MINE
                viewModel.getPartyFavorite(it)
            })
        }

        binding.rvPartyFavorite.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = EventHorizontalAdapter(context, listParty, clickListener = {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, it)
                context.startActivity(intent)
            }, clickFav = {
                path = PATH_FAVORITE
                viewModel.getPartyFavorite(it)
            })
        }

        binding.rvPartyJoined.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = EventHorizontalAdapter(context, listParty, clickListener = {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, it)
                context.startActivity(intent)
            }, clickFav = {
                path = PATH_JOINED
                viewModel.getPartyFavorite(it)
            })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun populateData() {
        viewModel.partyFavoriteResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            if (path.equals(PATH_MINE)) {
                                viewModel.getPartyMine(PATH_MINE)
                            } else if (path.equals(PATH_FAVORITE)) {
                                viewModel.getPartyFav(PATH_FAVORITE)
                            } else if (path.equals(PATH_JOINED)) {
                                viewModel.getPartyJoined(PATH_JOINED)
                            }
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }

            }
        })

        viewModel.partyFollowersResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.tvFollower.text = "Volgers (${it.size})"
                        binding.rvFollower.adapter?.let { adapter ->
                            when (adapter) {
                                is PartyFollowerAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {

                }
            }
        })

        viewModel.partyMineResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.tvPartyMine.text = "Georganiseerd (${it.size})"
                        binding.rvPartyMine.adapter?.let { adapter ->
                            when (adapter) {
                                is EventHorizontalAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.partyFavResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.tvPartyFavorite.text = "Favoriete feesten (${it.size})"
                        binding.rvPartyFavorite.adapter?.let { adapter ->
                            when (adapter) {
                                is EventHorizontalAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.partyJoinedResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.tvPartyJoined.text = "Aanwezig (${it.size})"
                        binding.rvPartyJoined.adapter?.let { adapter ->
                            when (adapter) {
                                is EventHorizontalAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }
}