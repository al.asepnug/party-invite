package com.muxe.partyinvite.ui.allEvent

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Location
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AllEventViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _partyResponse = MutableLiveData<ApiResponse<List<Party>>>()
    private val _partyNearbyResponse = MutableLiveData<ApiResponse<List<Party>>>()
    private val _partyFavoriteResponse = MutableLiveData<ApiResponse<Message>>()
    private val _partyIdResponse = MutableLiveData<ApiResponse<Party>>()

    val partyResponse = _partyResponse
    val partyNearbyResponse = _partyNearbyResponse
    val partyFavoriteResponse = _partyFavoriteResponse
    val partyIdResponse = _partyIdResponse

    fun getLocation(): Location = dataRepository.getLocation()

    fun getPartyById(id: Int?) {
        viewModelScope.launch {
            dataRepository.getPartyById(id).collect{
                _partyIdResponse.value = it
            }
        }
    }

    fun getParty(path: String?) {
        viewModelScope.launch {
            dataRepository.getParty(path).collect{
                _partyResponse.value = it
            }
        }
    }

    fun getPartyNearby(latitude: String?, longitude: String?) {
        viewModelScope.launch {
            dataRepository.getPartyNearby(latitude, longitude).collect{
                _partyNearbyResponse.value = it
            }
        }
    }

    fun getPartyFavorite(id: Int?) {
        viewModelScope.launch {
            dataRepository.getPartyFavorite(id).collect{
                _partyFavoriteResponse.value = it
            }
        }
    }

    fun getPartySearch(q: String, period: String) {
        viewModelScope.launch {
            dataRepository.getPartySearch(q, period).collect{
                _partyResponse.value = it
                _partyNearbyResponse.value = it
            }
        }
    }

}