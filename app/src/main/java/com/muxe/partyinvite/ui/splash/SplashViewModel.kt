package com.muxe.partyinvite.ui.splash

import androidx.lifecycle.ViewModel
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Location
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel(){

    fun getIsLogin(): Boolean = dataRepository.getIsLogin()

    fun setLocation(value: Location) = dataRepository.setLocation(value)

}