package com.muxe.partyinvite.ui.accountDetail

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.activity.viewModels
import androidx.core.content.FileProvider
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.button.MaterialButton
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.lyrebirdstudio.croppylib.Croppy
import com.lyrebirdstudio.croppylib.main.CropRequest
import com.lyrebirdstudio.croppylib.main.CroppyTheme
import com.lyrebirdstudio.croppylib.main.StorageType
import com.muxe.partyinvite.BuildConfig
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityAccountDetailBinding
import com.muxe.partyinvite.ui.participantsList.ParticipantsListActivity
import com.muxe.partyinvite.ui.paymentList.PaymentListActivity
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AccountDetailActivity : AppCompatActivity(), View.OnClickListener,
    UploadRequestBody.UploadCallback {
    private lateinit var binding: ActivityAccountDetailBinding
    private val viewModel: AccountDetailViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private var user: User? = null
    private var userDetails: UserDetails? = null
    private var currentPhotoPath: String? = null
    private var uriImage: Uri? = null
    private lateinit var dialog: Dialog
    private var isCamera: Boolean = false
    private lateinit var potoImage: File
    private val listFollowers = ArrayList<Owner>()
    private val listFollowing = ArrayList<Owner>()

    companion object {
        const val DATA_USER = "data_user"
        const val DATA_ACCOUNT = "data_account"
        private const val RC_CROP_IMAGE = 102
        private const val REQUEST_IMAGE_CAPTURE = 1
        private const val REQUEST_PICK_IMAGE = 2
        private const val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        // Inflate the layout for this fragment
        binding.btnSave.setOnClickListener(this)
        binding.btnEdit.setOnClickListener(this)
        binding.viewPayment.setOnClickListener(this)
        binding.viewFollowing.setOnClickListener(this)
        binding.viewFollowers.setOnClickListener(this)
        binding.btnBack.setOnClickListener(this)
        binding.btnEditPhoto.setOnClickListener(this)
        try {
            user = intent.extras?.getParcelable(DATA_USER)
            user?.id?.let { viewModel.getUser(it) }
            populateData()
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateUi() {
        viewModel.inserUser(
            User(
                userDetails?.id!!,
                userDetails?.avatar,
                userDetails?.firstname,
                userDetails?.email,
                userDetails?.lastname,
                userDetails?.ratingRaw,
                userDetails?.cover,
                userDetails?.rating,
                userDetails?.bio,
                userDetails?.ratingCount
            )
        )
        binding.btnSave.visibility = View.GONE
        userDetails?.avatar?.url?.let { showImageRounded(it, binding.imgProfile) }
        binding.tvName.setText("${userDetails?.firstname} ${userDetails?.lastname}")
        binding.tvEmail.setText(userDetails?.email)
        binding.tvBio.setText(userDetails?.bio)
        binding.tvName.isEnabled = false
        binding.tvEmail.isEnabled = false
        binding.tvTelphone.isEnabled = false
        binding.tvBio.isEnabled = false
    }

    private fun populateData() {
        viewModel.uploadResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let { it ->
                        if (it.error.equals("")) {
                            postProfile(it.id)
                        } else {
                            it.error?.let { it1 -> toast(it1) }
                        }
                    }

                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {

                }
            }
        })

        viewModel.messageResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let { it ->
                        makeSuccess(title = "Success", content = "Successfully " +
                                "${it.status?.replace('_', ' ')}", confirmListener = {
                            user?.id?.let { data -> viewModel.getUser(data) }
                        })
                    }

                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })

        viewModel.userResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        userDetails = it
                        listFollowers.clear()
                        it.followers?.let { data -> listFollowers.addAll(data) }
                        listFollowing.clear()
                        it.following?.let { data -> listFollowing.addAll(data) }
                        updateUi()
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnSave -> {
                if (binding.tvName.text.toString().isEmpty() && binding.tvEmail.text.toString()
                        .isEmpty() &&
                    binding.tvTelphone.text.toString().isEmpty()
                ) {
                    toast("Form is empty")
                } else if (!binding.tvEmail.text.toString().trim { it <= ' ' }
                        .matches(emailPattern.toRegex()) && binding.tvEmail.text.toString() != "admin") {
                    toast("Invalid email address")
                } else {
                    binding.tvName.isEnabled = false
                    binding.tvEmail.isEnabled = false
                    binding.tvTelphone.isEnabled = false
                    binding.tvBio.isEnabled = false
                    binding.btnEdit.visibility = View.VISIBLE
                    binding.btnEditPhoto.visibility = View.INVISIBLE
                    binding.btnSave.visibility = View.INVISIBLE
                    if (uriImage != null) {
                        uploadPicture()
                    } else {
                        postProfile(null)
                    }

                }
            }

            R.id.btnEdit -> {
                binding.tvName.isEnabled = true
                binding.tvEmail.isEnabled = true
                binding.tvTelphone.isEnabled = true
                binding.tvBio.isEnabled = true
                binding.btnEdit.visibility = View.INVISIBLE
                binding.btnEditPhoto.visibility = View.VISIBLE
                binding.btnSave.visibility = View.VISIBLE
            }

            R.id.btnEditPhoto -> {
                showDialogEdtPhoto()
            }

            R.id.viewPayment -> {
                val intent = Intent(this, PaymentListActivity::class.java)
                intent.putExtra(DATA_ACCOUNT, userDetails)
                startActivity(intent)
            }

            R.id.viewFollowers -> {
                val intent = Intent(this, ParticipantsListActivity::class.java)
                intent.putParcelableArrayListExtra(ParticipantsListActivity.DATA, listFollowers)
                intent.putExtra(ParticipantsListActivity.ID_USER, userDetails?.id)
                intent.putExtra(ParticipantsListActivity.TYPE_VIEW, "followers")
                startActivity(intent)
            }

            R.id.viewFollowing -> {
                val intent = Intent(this, ParticipantsListActivity::class.java)
                intent.putParcelableArrayListExtra(ParticipantsListActivity.DATA, listFollowing)
                intent.putExtra(ParticipantsListActivity.ID_USER, userDetails?.id)
                intent.putExtra(ParticipantsListActivity.TYPE_VIEW, "following")
                startActivity(intent)
            }

            R.id.btnBack -> {
                onBackPressed()
            }
        }
    }

    private fun showDialogEdtPhoto() {
        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_choose_image)
        val window: Window? = dialog.window
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val btnCamera = dialog.findViewById(R.id.btnCamera) as MaterialButton
        val btnGallery = dialog.findViewById(R.id.btnGallery) as MaterialButton
        val btnCancel = dialog.findViewById(R.id.btnCancel) as MaterialButton
        btnCamera.setOnClickListener {
            askForPermissions(true)
            dialog.dismiss()
        }

        btnGallery.setOnClickListener {
            askForPermissions(false)
            dialog.dismiss()
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
            intent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createCapturedPhoto()
                } catch (ex: IOException) {
                    // If there is error while creating the File, it will be null
                    null
                }

                photoFile?.also {
                    val photoURI = FileProvider.getUriForFile(
                        Objects.requireNonNull(this),
                        BuildConfig.APPLICATION_ID + ".provider", it
                    )

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createCapturedPhoto(): File {
        val timestamp: String = SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(Date())
        //val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val storageDir = cacheDir

        return File.createTempFile("PHOTO_${timestamp}", ".jpg", storageDir).apply {
            potoImage = this
            currentPhotoPath = absolutePath
        }
    }

    private fun openGallery() {
        Intent(Intent.ACTION_PICK).also {
            it.type = "image/*"
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            it.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(it, REQUEST_PICK_IMAGE)
        }
    }

    private fun postProfile(avatarId: Int?) {
        val body = JSONObject()
        val fullName: String = binding.tvName.text.toString()
        try {
            val index = fullName.lastIndexOf(' ')
            body.put("firstname", index.let { it1 -> fullName.substring(0, it1) })
            body.put("lastname", index.plus(1).let { it1 -> fullName.substring(it1) })
        } catch (e: Exception) {
            body.put("firstname", binding.tvName.text.toString())
            body.put("lastname", "")
        }
        body.put("email", binding.tvEmail.text.toString())
        body.put("bio", binding.tvBio.text.toString())
        if (avatarId != null) {
            body.put("avatar_id", avatarId)
        }
        viewModel.putUser(body.toString())
    }

    private fun uploadPicture() {
        if (uriImage == null) {
            toast("Select an Image First")
            return
        }
        try {
            val parcelFileDescriptor =
                contentResolver.openFileDescriptor(uriImage!!, "r", null) ?: return

            Log.d("parcelFileDescriptor", parcelFileDescriptor.toString())

            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            if (isCamera) {
//                val file = File(currentPhotoPath, contentResolver.getFileName(uriImage!!))
//                val outputStream = FileOutputStream(file)
//                inputStream.copyTo(outputStream)
                makeLoading(pDialog, true)
                uploadLoading(pDialog, "0 %")
                val body = UploadRequestBody(File(currentPhotoPath), "image", this)
                viewModel.postUploadMedia(
                    MultipartBody.Part.createFormData(
                        "file",
                        File(currentPhotoPath).name,
                        body
                    ), "avatar".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                )
            } else {
                val file = File(cacheDir, contentResolver.getFileName(uriImage!!))
                val outputStream = FileOutputStream(file)
                inputStream.copyTo(outputStream)
                makeLoading(pDialog, true)
                uploadLoading(pDialog, "0 %")
                val body = UploadRequestBody(file, "image", this)
                viewModel.postUploadMedia(
                    MultipartBody.Part.createFormData(
                        "file",
                        file.name,
                        body
                    ), "avatar".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                )
            }

        } catch (exc: Exception) {
            Log.d("akundetail", exc.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                REQUEST_IMAGE_CAPTURE -> {
                    val uri = Uri.fromFile(File(currentPhotoPath))
                    isCamera = true
                    startCroppy(uri)
                }
                REQUEST_PICK_IMAGE -> {
                    data?.data?.let {
                        isCamera = false
                        startCroppy(it)
                    }
                }
                RC_CROP_IMAGE -> {
                    data?.data?.let {
                        binding.imgProfile.setImageURI(it)
                        uriImage = it

                    }
                }
            }
        }
    }

    private fun startCroppy(uri: Uri) {
        val cacheCropRequest = CropRequest.Auto(
            sourceUri = uri,
            requestCode = RC_CROP_IMAGE,
            storageType = StorageType.CACHE,
            croppyTheme = CroppyTheme(R.color.orange_500)
        )
        Croppy.start(this, cacheCropRequest)
    }

    private fun askForPermissions(isCamera: Boolean) {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                openCamera()
                            } else {
                                openGallery()
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            makeWarning(title = "Camera- en opslagvergunning vereist ",
                                content = "Gebruik van toestaan via instellingen?",
                                confirmListener = {
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts(
                                        "package",
                                        packageName,
                                        null
                                    )
                                    intent.data = uri
                                    startActivity(intent)
                                    dialog.dismiss()
                                },
                                cancelListener = {
                                    dialog.dismiss()
                                })
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    // Remember to invoke this method when the custom rationale is closed
                    // or just by default if you don't want to use any custom rationale.
                    token?.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Log.d("Account Detail", it.name)
            }
            .check()
    }

    override fun onProgressUpdate(percentage: Int) {
        uploadLoading(pDialog, "$percentage %")
    }

}