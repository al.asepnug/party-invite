package com.muxe.partyinvite.ui.pageEvent

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.ui.pageEvent.PageEvent.Companion.DATA
import com.muxe.partyinvite.ui.pageEvent.detailsPageEvent.DetailsPageEventFragment
import com.muxe.partyinvite.ui.pageEvent.messagePageEvent.MessagePageEventFragment
import com.muxe.partyinvite.ui.pageEvent.preMessageEvent.PreMessagePageEventFragment

class ViewPagerEventFragmentAdapter(fm: FragmentManager, lifecycle: Lifecycle,
                                    private var numberOfTabs: Int, private var party: Party)
    : FragmentStateAdapter(fm, lifecycle) {

    override fun createFragment(position: Int): Fragment {
        val bundle = Bundle()
        bundle.putParcelable(DATA, party)
        when (position) {
            0 -> {
                val myObj = DetailsPageEventFragment()
                myObj.arguments = bundle
                return myObj
            }
            1 -> {
                val myObj = MessagePageEventFragment()
                myObj.arguments = bundle
                return myObj
            }
            else -> {
                val myObj = PreMessagePageEventFragment()
                myObj.arguments = bundle
                return myObj
            }
        }
    }

    override fun getItemCount(): Int {
        return numberOfTabs
    }
}