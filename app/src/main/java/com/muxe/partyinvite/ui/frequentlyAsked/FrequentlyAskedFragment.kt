package com.muxe.partyinvite.ui.frequentlyAsked

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.muxe.partyinvite.data.entity.Faq
import com.muxe.partyinvite.databinding.FragmentFrequentlyAskedBinding
import com.muxe.partyinvite.utils.ValueFormat.getJsonDataFromAsset
import com.muxe.partyinvite.utils.toast
import java.io.IOException

class FrequentlyAskedFragment : Fragment() {
    private val binding by lazy { FragmentFrequentlyAskedBinding.inflate(layoutInflater) }
    private val listFaq = ArrayList<Faq>()

    companion object {
        fun newInstance() = FrequentlyAskedFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initUi()
        return binding.root
    }

    private fun initUi() {
        binding.rvFaq.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = FaqAdapter(listFaq)
        }

        val jsonFileString = getJsonDataFromAsset(requireContext(), "faq.json")
        if (jsonFileString != null) {
            val gson = Gson()
            val listFaqType = object : TypeToken<List<Faq>>() {}.type
            val faqList: List<Faq> = gson.fromJson(jsonFileString, listFaqType)
            binding.rvFaq.adapter?.let { adapter ->
                when (adapter) {
                    is FaqAdapter -> {
                        try {
                            adapter.updateData(faqList)
                        } catch (e: IOException) {
                            requireActivity().toast("Error Load Data")
                        }
                    }
                }
            }
        }
    }
}