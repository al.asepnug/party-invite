package com.muxe.partyinvite.ui.frequentlyAsked

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.data.entity.Faq
import com.muxe.partyinvite.databinding.ItemFaqBinding

class FaqAdapter(private var faqList: ArrayList<Faq>
) : RecyclerView.Adapter<FaqAdapter.ViewHolder>() {

    inner class ViewHolder(val binding: ItemFaqBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFaqBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder){
            with(faqList[position]){
                binding.tvTitle.text = this.title
                binding.tvDescription.text = this.description
                binding.expandedView.visibility = if (this.expand) View.VISIBLE else View.GONE
                itemView.setOnClickListener {
                    this.expand = !this.expand
                    notifyDataSetChanged()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return faqList.size
    }

    fun updateData(newList: List<Faq>) {
        faqList.clear()
        faqList.addAll(newList)
        notifyDataSetChanged()
    }
}