package com.muxe.partyinvite.ui.myTicket

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.data.entity.Tickets
import com.muxe.partyinvite.databinding.ItemTicketBinding
import com.muxe.partyinvite.utils.ValueFormat

class MyTicketAdapter (private val context: Context, private val listItem: ArrayList<Tickets>,
                    private val clickListener: (Tickets) -> Unit)
    : RecyclerView.Adapter<MyTicketAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemTicketBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listItem[position])
    }

    inner class ViewHolder(private val binding: ItemTicketBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: Tickets) {
            binding.tvTitle.text = data.party?.label
            binding.tvPlace.text = "${data.party?.address}, ${data.party?.postalcode}, ${data.party?.city}"
            binding.tvDate.text = ValueFormat.formatDate(data.date)
            binding.tvTime.text = ValueFormat.formatTime(data.date)
            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Tickets>) {
        listItem.clear()
        listItem.addAll(newList)
        notifyDataSetChanged()
    }
}