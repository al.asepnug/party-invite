package com.muxe.partyinvite.ui.allEvent

import android.Manifest
import android.annotation.SuppressLint
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.viewModels
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.databinding.FragmentForYouEventBinding
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.utils.*
import com.muxe.partyinvite.utils.ValueFormat.bitmapDescriptorFromVector
import com.muxe.partyinvite.utils.ValueFormat.formatRound
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

@AndroidEntryPoint
class ForYouEventFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    View.OnClickListener {
    private val binding by lazy { FragmentForYouEventBinding.inflate(layoutInflater) }
    private val viewModel: AllEventViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private lateinit var dataParty: Party
    private var listParty: List<Party>? = null
    private val listNearbyParty = ArrayList<Party>()

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    //private var markerMe: Marker? = null
    private var latitude: String = "52.373000"
    private var longitude: String = "4.892397"
    private var myMarkers = ArrayList<Marker?>()

    companion object {
        //        fun newInstance() = EventAllFragment()
        private const val TAG = "ForYouEventFragment"
        const val DATA_PARTY = "data_party"
        fun newInstance(data: ArrayList<Party>): ForYouEventFragment {
            val fragment = ForYouEventFragment()

            val bundle = Bundle().apply {
                putParcelableArrayList(DATA_PARTY, data)
            }

            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        pDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.PROGRESS_TYPE)
//        binding.layMarkerDetail.layMarkerDetail.setOnClickListener(this)
        binding.layMarkerDetail.layMarkerDetail.setOnClickListener(this)
        binding.layMarkerDetail.imgFav.setOnClickListener(this)
        binding.layMarkerDetail.imgClose.setOnClickListener(this)
        binding.layMarkerDetail.layMarkerDetail.visibility = View.GONE
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //(activity?.supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment?)?.getMapAsync(this)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        try {
            if (arguments?.containsKey(DATA_PARTY) == true) {
                listParty = arguments?.getParcelableArrayList(DATA_PARTY)
            }
        } catch (e: IOException) {
            requireActivity().toast("Error Load Data")
        }
        populateData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val listItem: MenuItem? = menu.findItem(R.id.action_list)
        listItem?.isVisible = true
        val mapsItem: MenuItem? = menu.findItem(R.id.action_maps)
        mapsItem?.isVisible = false

        val searchItem: MenuItem? = menu.findItem(R.id.action_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as androidx.appcompat.widget.SearchView
            searchView.setOnCloseListener { true }
            val searchPlate =
                searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
            searchPlate.hint = "Zoeken Alle Festen"
            val searchPlateView: View =
                searchView.findViewById(androidx.appcompat.R.id.search_plate)
            searchPlateView.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.transparent
                )
            )

            searchView.setOnQueryTextListener(DebouncingQueryTextListener(
                requireActivity()
            ) { newText ->
                newText?.let {
                    if (it.isNotEmpty()) {
                        viewModel.getPartySearch(it, "period")
                    }
                }
            })

            searchView.setOnCloseListener {
                if (listParty != null) {
                    viewModel.getParty("")
                } else {
                    viewModel.getPartyNearby(latitude, longitude)
                }
                searchView.onActionViewCollapsed()
                false
            }
            val searchManager =
                requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
        }
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_maps -> {
                val bundle = Bundle()
                bundle.putParcelableArrayList(DATA_PARTY, listNearbyParty)
                binding.fragmentContainer.visibility = View.GONE
//                childFragmentManager.commit {
//                    setReorderingAllowed(false)
//                    replace<ForYouEventFragment>(R.id.fragment_container, args = bundle)
//                }
                childFragmentManager.popBackStack()
                true
            }
            R.id.action_list -> {
                val bundle = Bundle()
                bundle.putParcelableArrayList(DATA_PARTY, listNearbyParty)
//                bundle.putString("latitude", latitude)
//                bundle.putString("longitude", longitude)
                binding.fragmentContainer.visibility = View.VISIBLE
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    addToBackStack(null)
                    replace<EventAllFragment>(R.id.fragment_container, args = bundle)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun populateData() {
        viewModel.partyFavoriteResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        requireActivity().makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            viewModel.getPartyById(it.id)
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.partyIdResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        if (binding.layMarkerDetail.layMarkerDetail.visibility == View.GONE) {
                            binding.layMarkerDetail.layMarkerDetail.visibility = View.VISIBLE
                        }
                        updateUiMarkerDetail(it)
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.partyNearbyResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    //requireActivity().makeLoading(pDialog, false)
                    result.body?.let { it ->
                        if (it.size > 0) {
                            listNearbyParty.clear()
                            listNearbyParty.addAll(it)
                            GlobalScope.launch(Dispatchers.Main) {
//                                updateUiMarkerDetail(it[0])
//                                dataParty = it[0]
                                it.forEach { data ->
                                    if (data.latitude != null && data.longitude != null) {
                                        val marker: Marker? = mMap.addMarker(
                                            MarkerOptions().position(
                                                LatLng(
                                                    data.latitude.toDouble(),
                                                    data.longitude.toDouble()
                                                )
                                            ).title(data.id.toString())
                                                .icon(
                                                    bitmapDescriptorFromVector(
                                                        requireContext(),
                                                        R.drawable.ic_pin
                                                    )
                                                )
                                        )
                                        marker?.tag = data
                                        marker?.hideInfoWindow()
                                        myMarkers.add(marker)
                                    }
                                }
                            }
                        } else {
                            requireActivity().toast("Data is Empty")
                            binding.layMarkerDetail.layMarkerDetail.visibility = View.GONE
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude.toDouble(),
                                longitude.toDouble()), 10.0f))
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    //requireActivity().makeLoading(pDialog, false)
                    result.message?.let { requireActivity().toast(it) }
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude.toDouble(),
                        longitude.toDouble()), 10.0f))
                }

                ApiResponse.StatusResponse.LOADING -> {
                    //requireActivity().makeLoading(pDialog, true)
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.layMarkerDetail -> {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, dataParty)
                context?.startActivity(intent)
            }
            R.id.imgFav -> {
                dataParty.id.let {
                    viewModel.getPartyFavorite(it)
                }
            }
            R.id.imgClose -> {
                binding.layMarkerDetail.layMarkerDetail.visibility = View.GONE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (::mMap.isInitialized) {
            //initLocationTracking()
            Log.d(TAG, "onResume")
        }
    }

    override fun onPause() {
        super.onPause()
        if (::fusedLocationClient.isInitialized && ::locationCallback.isInitialized) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    @SuppressLint("PotentialBehaviorOverride")
    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.let {
            Log.d(TAG, "onMapReady")
            mMap = it
            mMap.setOnMarkerClickListener(this)
            initMap()
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        if (marker.tag != null) {
            if (binding.layMarkerDetail.layMarkerDetail.visibility == View.GONE) {
                binding.layMarkerDetail.layMarkerDetail.visibility = View.VISIBLE
            }
            val data: Party = marker.tag as Party
            dataParty = data
            updateUiMarkerDetail(data)
        }
        return true
    }

    private fun askForPermissions() {
        Dexter.withActivity(requireActivity())
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            if (locationEnabled()) {
                                loadMap()
                            } else {
                                requireActivity().makeWarning(title = "GPS moet zijn ingeschakeld",
                                    content = "GPS inschakelen via instellingen?",
                                    confirmListener = {
                                        val intent =
                                            Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                                        startActivity(intent)
                                        requireActivity().finish()
                                    },
                                    cancelListener = {
                                        requireActivity().finish()
                                    })
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            requireActivity().makeWarning(title = "Locatievergunning vereist",
                                content = "Gebruik van locatie toestaan via instellingen?",
                                confirmListener = {
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts(
                                        "package",
                                        requireActivity().packageName,
                                        null
                                    )
                                    intent.data = uri
                                    startActivity(intent)
                                    requireActivity().finish()
                                },
                                cancelListener = {
                                    requireActivity().finish()
                                })
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    // Remember to invoke this method when the custom rationale is closed
                    // or just by default if you don't want to use any custom rationale.
                    token?.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Log.d(TAG, it.name)
            }
            .check()
    }

    private fun locationEnabled(): Boolean {
        val locationManager =
            requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(requireActivity())
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                requireActivity().makeWarning(title = "Vereist Google Play-services",
                    content = "Download Google Play-services in de Play Store",
                    confirmListener = {
                        val intent = Intent(Intent.ACTION_VIEW).apply {
                            data = Uri.parse(
                                "https://play.google.com/store/apps/details?id=com.example.android"
                            )
                            setPackage("com.android.vending")
                        }
                        startActivity(intent)
                        requireActivity().finish()
                    },
                    cancelListener = {
                        requireActivity().finish()
                    })
            } else {
                requireActivity().toast("Dit apparaat is niet beschikbaar Google Play-service")
                requireActivity().finish()
            }
            return false
        }
        return true
    }

    //init map & populate data
    @SuppressLint("SetTextI18n")
    private fun initMap() {
        Log.d(TAG, "initmap")
        mMap.uiSettings.isZoomControlsEnabled = false
        //mMap.setOnMarkerClickListener(this)
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(52.373000, 4.892397), 12.0f))
        if (checkPlayServices()) {
            askForPermissions()
        }
    }

    private fun loadMap() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        // Add a marker and move the camera

        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(requireActivity()) { location ->
            if (location != null) {
                val currentLatLng = LatLng(location.latitude, location.longitude)
                latitude = location.latitude.toString()
                longitude = location.longitude.toString()
                //debug
                //val currentLatLng = LatLng(52.373000,4.892397)
                if (listParty != null) {
                    if (listParty!!.isNotEmpty()) {
                        binding.layMarkerDetail.layMarkerDetail.visibility = View.VISIBLE
                        GlobalScope.launch(Dispatchers.Main) {
//                            updateUiMarkerDetail(listParty!![0])
//                            dataParty = listParty!![0]
                            listParty!!.forEach { data ->
                                if (data.latitude != null && data.longitude != null) {
                                    val marker: Marker? = mMap.addMarker(
                                        MarkerOptions().position(
                                            LatLng(
                                                data.latitude.toDouble(),
                                                data.longitude.toDouble()
                                            )
                                        ).title(data.id.toString())
                                            .icon(
                                                bitmapDescriptorFromVector(
                                                    requireContext(),
                                                    R.drawable.ic_pin
                                                )
                                            )
                                    )
                                    marker?.tag = data
                                    marker?.hideInfoWindow()
                                    myMarkers.add(marker)
                                }
                            }
                        }
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12.0f))
                    } else {
                        requireActivity().toast("Data is Empty")
                        binding.layMarkerDetail.layMarkerDetail.visibility = View.GONE
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 10.0f))
                    }
                } else {
                    //debug
//                    viewModel.getPartyNearby(
//                        latitude, longitude
//                    )
                    viewModel.getPartyNearby(
                        location.latitude.toString(),
                        location.longitude.toString()
                    )
                }
//                initLocationTracking()
            }
        }
    }

    private fun initLocationTracking() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    updateMapLocation(location)
                }
            }
        }

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        val locationRequest = LocationRequest.create().apply {
            interval = 100
            fastestInterval = 50
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            maxWaitTime = 100
        }

        try {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                null
            )
        } catch (exc: Exception) {
            requireActivity().makeWarning(title = "GPS moet zijn ingeschakeld",
                content = "GPS inschakelen via instellingen?",
                confirmListener = {
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(intent)
                    requireActivity().finish()
                },
                cancelListener = {
                    requireActivity().finish()
                })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateMapLocation(location: Location?) {
        if (location != null) {
            val currentLatLng = LatLng(location.latitude, location.longitude)
            //debug 52.373000, 4.892397
           // val currentLatLng = LatLng(52.373000, 4.892397)
            latitude = currentLatLng.latitude.toString()
            longitude = currentLatLng.longitude.toString()
//            if (markerMe != null) {
//                markerMe!!.remove()
//                markerMe = null
//            }
//
//            if (markerMe == null) {
//                markerMe = mMap.addMarker(
//                    MarkerOptions().position(currentLatLng).title("Jouw locatie")
//                )
//            }
        } else {
            initLocationTracking()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateUiMarkerDetail(data: Party) {
        binding.layMarkerDetail.tvTitle.text = data.label
        binding.layMarkerDetail.tvPlace.text = "${data.address}, ${data.postalcode}, ${data.city}"
        binding.layMarkerDetail.tvDistance.text = "${formatRound(data.distance)} km afstand"
        binding.layMarkerDetail.tvDate.text = ValueFormat.formatDateTime(data.date)
        binding.layMarkerDetail.tvPrice.text = "€ ${data.price}"
        data.cover?.url?.let {
            context?.showImageCenterInside(
                it,
                binding.layMarkerDetail.imgContent
            )
        }
        binding.layMarkerDetail.tvFav.text = data.favoritedCount.toString()
        if (data.isFavorite == true) {
            binding.layMarkerDetail.tvFav.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.black
                )
            )
            binding.layMarkerDetail.imgFav.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_favorite_24
                )
            )
        } else {
            binding.layMarkerDetail.tvFav.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white
                )
            )
            binding.layMarkerDetail.imgFav.setImageDrawable(
                ContextCompat.getDrawable(
                    requireContext(),
                    R.drawable.ic_baseline_favorite_border_24
                )
            )
        }
    }

}