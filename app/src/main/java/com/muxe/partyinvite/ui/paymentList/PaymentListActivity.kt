package com.muxe.partyinvite.ui.paymentList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.PaymentsItem
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.databinding.ActivityPaymentListBinding
import com.muxe.partyinvite.ui.accountDetail.AccountDetailActivity.Companion.DATA_ACCOUNT
import com.muxe.partyinvite.utils.toast
import java.io.IOException

class PaymentListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPaymentListBinding
    private val paymentList = ArrayList<PaymentsItem>()
    private lateinit var user: UserDetails

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.title = "Betalingsgegevens"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        try {
            initUi()
            user = intent.extras?.getParcelable(DATA_ACCOUNT)!!
            initUi()
            user.payments.let {
                if (it != null) {
                    binding.rvPayments.adapter?.let { adapter ->
                        when (adapter) {
                            is PaymentListAdapter -> {
                                try {
                                    adapter.updateData(it)
                                } catch (e: IOException) {
                                    toast("Error Load Data")
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initUi() {
        binding.rvPayments.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = PaymentListAdapter(context, paymentList, clickListener = {
            })
        }
    }
}