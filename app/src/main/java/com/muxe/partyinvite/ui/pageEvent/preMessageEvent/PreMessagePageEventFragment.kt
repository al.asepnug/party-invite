package com.muxe.partyinvite.ui.pageEvent.preMessageEvent

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Messages
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.FragmentPreMessagePageEventBinding
import com.muxe.partyinvite.ui.followersDetail.FollowersDetailActivity
import com.muxe.partyinvite.ui.pageEvent.MessagesAdapter
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.ui.pageEvent.PageEventViewModel
import com.muxe.partyinvite.ui.pageEvent.messagePageEvent.MessagePageEventFragment
import com.muxe.partyinvite.utils.makeLoading
import com.muxe.partyinvite.utils.makeSuccess
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class PreMessagePageEventFragment : Fragment(), View.OnClickListener {
    private val binding by lazy { FragmentPreMessagePageEventBinding.inflate(layoutInflater) }
    private val viewModel: PageEventViewModel by viewModels()
    private val listMessages = ArrayList<Messages>()
    private var party: Party? = null
    private lateinit var pDialog: SweetAlertDialog

    companion object {
        private const val ARG_NAME = PageEvent.DATA
        const val TYPE_MSG = "pre"
        fun newInstance(data: Messages): PreMessagePageEventFragment {
            val fragment = PreMessagePageEventFragment()

            val bundle = Bundle().apply {
                putParcelable(ARG_NAME, data)
            }

            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        pDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.PROGRESS_TYPE)
        binding.btnAddMsg.setOnClickListener(this)
        try {
            party = arguments?.getParcelable(MessagePageEventFragment.ARG_NAME)
            if (party !=null) {
                viewModel.getMessagesPreParty(TYPE_MSG, party?.id)
            } else {
                requireActivity().toast("Error Load Data")
            }
        } catch (e: IOException) {
            requireActivity().toast("Error Load Data")
        }
        initUi()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateData()
    }

    private fun initUi() {
        binding.rvMessage.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = MessagesAdapter(context, listMessages, likeClickListener = {
                if (it != null) {
                    viewModel.postLikeMessages("like", it)
                } else {
                    requireActivity().toast("vind je al leuk")
                }
            }, dislikeClickListener = {
                if (it != null) {
                    viewModel.postLikeMessages("dislike", it)
                } else {
                    requireActivity().toast("vind je al afkeer")
                }
            }, profileClickListener = {
                val intent = Intent(context, FollowersDetailActivity::class.java)
                intent.putExtra(FollowersDetailActivity.DATA, it)
                context.startActivity(intent)
            })
        }
    }

    private fun populateData() {
        viewModel.postMessagesResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.body?.let {
                        requireActivity().makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            viewModel.getMessagesPreParty(TYPE_MSG, party?.id)
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    requireActivity().makeLoading(pDialog, true)
                }
            }
        })

        viewModel.likeResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.body?.let {
                        requireActivity().makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            viewModel.getMessagesPreParty(TYPE_MSG, party?.id)
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    requireActivity().makeLoading(pDialog, true)
                }
            }
        })

        viewModel.messagesPreResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvMessage.adapter?.let { adapter ->
                            when (adapter) {
                                is MessagesAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it.asReversed())
                                        } else {
                                            requireActivity().toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        requireActivity().toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnAddMsg -> {
                party?.id?.let { showDialogMsg(it) }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDialogMsg(idParty: Int) {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_add_message)
        val window: Window? = dialog.window
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val title = dialog.findViewById(R.id.tvTitle) as TextView
        val yesBtn = dialog.findViewById(R.id.btnSave) as MaterialButton
        val noBtn = dialog.findViewById(R.id.btnCancel) as MaterialButton
        val edtMessage = dialog.findViewById(R.id.edtDescriptionVal) as TextInputEditText
        title.text = "Pre-Party toevoegen"
        yesBtn.setOnClickListener {
            if (edtMessage.text.toString().isEmpty()) {
                requireActivity().toast("Form is empty")
            } else {
                viewModel.postMessages(TYPE_MSG, idParty, edtMessage.text.toString())
            }
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }
}