package com.muxe.partyinvite.ui.myTicket

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Tickets
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MyTicketViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _ticketsResponse = MutableLiveData<ApiResponse<List<Tickets>>>()

    val ticketsResponse = _ticketsResponse

    fun getTickets() {
        viewModelScope.launch {
            dataRepository.getTickets().collect{
                _ticketsResponse.value = it
            }
        }
    }
}