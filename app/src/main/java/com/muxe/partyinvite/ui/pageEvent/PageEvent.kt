package com.muxe.partyinvite.ui.pageEvent

import android.annotation.SuppressLint
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityPageEventBinding
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class PageEvent : AppCompatActivity(){
    private lateinit var binding: ActivityPageEventBinding
    private lateinit var viewPagerAdapter: ViewPagerEventFragmentAdapter
    private lateinit var party: Party
    private val viewModel: PageEventViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog

    companion object {
        const val DATA = "data_party"
        val titles = arrayOf("Details", "Berichten", "Pre-Party")
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPageEventBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        try {
            val data: Uri? = intent?.data
            if (data != null) {
                val idParty = data.lastPathSegment
                if (idParty.isNullOrBlank().not()) {
                    populateData()
                    viewModel.getPartyById(idParty?.toInt())
                }
            } else {
                party = intent.extras?.getParcelable(DATA)!!
                supportActionBar?.title = party.label
                initUi()
            }
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun initUi() {
        binding.tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.white))
        //binding.tabLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBackground))
        //binding.tabLayout.tabTextColors = ContextCompat.getColorStateList(context, android.R.color.white)
        binding.tabLayout.tabMode = TabLayout.MODE_FIXED
        viewPagerAdapter = ViewPagerEventFragmentAdapter(supportFragmentManager , lifecycle, titles.size, party)
        binding.viewPager.adapter = viewPagerAdapter
        binding.viewPager.isUserInputEnabled = false

        TabLayoutMediator(binding.tabLayout, binding.viewPager
        ) { tab, position ->
            tab.text = titles[position]
        }.attach()
    }

    private fun populateData() {
        viewModel.partyResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        party = it
                        supportActionBar?.title = party.label
                        initUi()
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }
}