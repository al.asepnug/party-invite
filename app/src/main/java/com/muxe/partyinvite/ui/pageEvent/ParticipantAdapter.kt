package com.muxe.partyinvite.ui.pageEvent

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.databinding.ItemParticipantBinding
import com.muxe.partyinvite.utils.showImageCircleCrop

class ParticipantAdapter (private val context: Context, private val participantsListItem: ArrayList<Owner>,
                          private val clickListener: (Owner) -> Unit)
    : RecyclerView.Adapter<ParticipantAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemParticipantBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return participantsListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(participantsListItem[position])
    }

    inner class ViewHolder(private val binding: ItemParticipantBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Owner) {
            data.avatar?.url?.let { context.showImageCircleCrop(it, binding.imageView) }

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Owner>?) {
        participantsListItem.clear()
        newList?.let { participantsListItem.addAll(it) }
        notifyDataSetChanged()
    }
}