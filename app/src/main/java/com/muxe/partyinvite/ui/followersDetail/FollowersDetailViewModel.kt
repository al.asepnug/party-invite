package com.muxe.partyinvite.ui.followersDetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Location
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.Reviews
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class FollowersDetailViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _messageResponse = MutableLiveData<ApiResponse<Message>>()
    private val _userDetailsResponse = MutableLiveData<ApiResponse<UserDetails>>()
    private val _partyFavoriteResponse = MutableLiveData<ApiResponse<Message>>()
    private val _reviewsResponse = MutableLiveData<ApiResponse<Message>>()

    val messageResponse = _messageResponse
    val userDetailsResponse = _userDetailsResponse
    val partyFavoriteResponse = _partyFavoriteResponse
    val reviewsResponse = _reviewsResponse

    fun getLocation(): Location = dataRepository.getLocation()

    fun postFollow(idUser: Int?) {
        viewModelScope.launch {
            dataRepository.postFollow(idUser).collect{
                _messageResponse.value = it
            }
        }
    }

    fun deleteFollow(idUser: Int?) {
        viewModelScope.launch {
            dataRepository.deleteFollow(idUser).collect{
                _messageResponse.value = it
            }
        }
    }

    fun getFullProfile(idUser: Int?) {
        viewModelScope.launch {
            dataRepository.getFullProfile(idUser).collect{
                _userDetailsResponse.value = it
            }
        }
    }

    fun getPartyFavorite(id: Int?) {
        viewModelScope.launch {
            dataRepository.getPartyFavorite(id).collect{
                _partyFavoriteResponse.value = it
            }
        }
    }

    fun postReviews(fields: HashMap<String?, Any?>) {
        viewModelScope.launch {
            dataRepository.postReviews(fields).collect{
                _reviewsResponse.value = it
            }
        }
    }
}