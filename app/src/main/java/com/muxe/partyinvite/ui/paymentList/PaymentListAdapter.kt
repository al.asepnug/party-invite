package com.muxe.partyinvite.ui.paymentList

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.data.entity.PaymentsItem
import com.muxe.partyinvite.databinding.ItemPaymentsBinding
import com.muxe.partyinvite.utils.ValueFormat

class PaymentListAdapter (private val context: Context, private val listItem: ArrayList<PaymentsItem>,
                       private val clickListener: (PaymentsItem) -> Unit)
    : RecyclerView.Adapter<PaymentListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPaymentsBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listItem[position])
    }

    inner class ViewHolder(private val binding: ItemPaymentsBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: PaymentsItem) {
            binding.tvTransactionId.text = data.transactionId
            binding.tvDate.text = ValueFormat.formatDateTimed(data.date)
            binding.tvProvider.text = data.provider
            binding.tvStatus.text = data.status
            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<PaymentsItem>) {
        listItem.clear()
        listItem.addAll(newList)
        notifyDataSetChanged()
    }
}