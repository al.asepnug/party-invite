package com.muxe.partyinvite.ui.allEvent

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.muxe.partyinvite.R
import com.muxe.partyinvite.databinding.AllEventFragmentBinding

class AllEventFragment : Fragment() {
    private val binding by lazy { AllEventFragmentBinding.inflate(layoutInflater) }
    private lateinit var viewPagerAdapter: ViewPagerFragmentAdapter

    companion object {
        fun newInstance() = AllEventFragment()
        val titles = arrayOf("Alle Feesten", "Special Voor Jouw")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initUi()
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun initUi() {
        binding.tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(requireContext(), R.color.white))
        //binding.tabLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBackground))
        //binding.tabLayout.tabTextColors = ContextCompat.getColorStateList(context, android.R.color.white)
        binding.tabLayout.tabMode = TabLayout.MODE_FIXED
        viewPagerAdapter = ViewPagerFragmentAdapter(requireActivity().supportFragmentManager, lifecycle, titles.size)
        binding.viewPager.adapter = viewPagerAdapter
        binding.viewPager.isUserInputEnabled = false

        TabLayoutMediator(binding.tabLayout, binding.viewPager
        ) { tab, position ->
            tab.text = titles[position]
        }.attach()
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        binding.appBarLayout.toolbar.title = getString(R.string.toolbar_name)
//
//        binding.appBarLayout.toolbar.inflateMenu(R.menu.main)
//
//        val searchItem: MenuItem = binding.appBarLayout.toolbar.menu.findItem(R.id.action_search)
//        val searchView = searchItem.actionView as androidx.appcompat.widget.SearchView
//        searchView.setOnCloseListener { true }
//        val searchPlate = searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
//        searchPlate.hint = "Search Feesten"
//        val searchPlateView: View =
//            searchView.findViewById(androidx.appcompat.R.id.search_plate)
//        searchPlateView.setBackgroundColor(
//            ContextCompat.getColor(
//                requireContext(),
//                android.R.color.transparent
//            )
//        )
//
//        searchView.setOnQueryTextListener(DebouncingQueryTextListener(
//            requireActivity()
//        ) { newText ->
//            newText?.let {
//                if (it.isEmpty()) {
//                    //binding.searchContent.visibility = View.GONE
//                    //binding.contentLay.visibility = View.VISIBLE
//                } else {
//                    requireActivity().toast(it)
//                    //getSearchPosts(it)
//                }
//            }
//        })
//
//        searchView.setOnCloseListener {
//            searchView.onActionViewCollapsed()
//            false
//        }
//        val searchManager = requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
//
//    }
}