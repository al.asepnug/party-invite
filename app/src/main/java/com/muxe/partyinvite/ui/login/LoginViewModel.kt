package com.muxe.partyinvite.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.*
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlinx.coroutines.flow.collect
import okhttp3.RequestBody

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _registerResponse = MutableLiveData<ApiResponse<Register?>>()
    private val _loginResponse = MutableLiveData<ApiResponse<Login?>>()
    private val _userResponse = MutableLiveData<ApiResponse<User>>()
    private val _facebookResponse = MutableLiveData<ApiResponse<FacebookResponse?>>()

    val registerResponse = _registerResponse
    val loginResponse = _loginResponse
    val userResponse = _userResponse
    val facebookResponse = _facebookResponse

    fun postLogin(body: String?) {
        viewModelScope.launch {
            dataRepository.postLogin(body).collect{
                _loginResponse.value = it
            }
        }
    }

    fun getUser() {
        viewModelScope.launch {
            dataRepository.getProfile().collect{
                _userResponse.value = it
            }
        }
    }

    fun postRegister(fields: HashMap<String?, RequestBody?>) {
        viewModelScope.launch {
            dataRepository.postRegister(fields).collect{
                _registerResponse.value = it
            }
        }
    }

    fun getFacebookData(userId: String, authHeader: String) {
        viewModelScope.launch {
            dataRepository.getFacebookData(userId, authHeader).collect{
                _facebookResponse.value = it
            }
        }
    }

}