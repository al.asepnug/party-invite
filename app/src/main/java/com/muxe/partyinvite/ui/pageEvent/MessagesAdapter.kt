package com.muxe.partyinvite.ui.pageEvent

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Messages
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.databinding.ItemEventMessageBinding
import com.muxe.partyinvite.ui.accountInfo.AccountInfoActivity
import com.muxe.partyinvite.utils.ValueFormat.formatDateTimed2
import com.muxe.partyinvite.utils.addOnClickListener
import com.muxe.partyinvite.utils.showImageCircleCrop

class MessagesAdapter (private val context: Context, private val messagesListItem: ArrayList<Messages>,
                       private val likeClickListener: (Int?) -> Unit,
                       private val dislikeClickListener: (Int?) -> Unit,
                       private val profileClickListener: (Owner?) -> Unit)
    : RecyclerView.Adapter<MessagesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemEventMessageBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return messagesListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(messagesListItem[position])
    }

    inner class ViewHolder(private val binding: ItemEventMessageBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: Messages) {
            data.owner?.avatar?.url?.let { context.showImageCircleCrop(it, binding.imgAvatar) }
            binding.tvName.text = "${data.owner?.firstname} ${data.owner?.lastname}"
            binding.tvDate.text = formatDateTimed2(data.date)
            binding.tvMessage.text = data.message
            binding.tvLike.text = data.likes
            binding.tvDisLike.text = data.dislikes
            if (data.likeState == -1) {
                ImageViewCompat.setImageTintList(binding.btnDisLike, ColorStateList.valueOf(
                    ContextCompat.getColor(context, R.color.red)))
            } else if (data.likeState == 1){
                ImageViewCompat.setImageTintList(binding.btnLike, ColorStateList.valueOf(
                    ContextCompat.getColor(context, R.color.green)))
            }

            binding.btnLike.setOnClickListener {
                if (data.likeState != 1) {
                    likeClickListener(data.id)
                } else {
                    likeClickListener(null)
                }
            }

            binding.btnDisLike.setOnClickListener {
                if (data.likeState != -1) {
                    dislikeClickListener(data.id)
                } else {
                    dislikeClickListener(null)
                }
            }

            binding.groupProfile.addOnClickListener{
                profileClickListener(data.owner)
            }
        }
    }

    fun updateData(newList: List<Messages>?) {
        messagesListItem.clear()
        newList?.let { messagesListItem.addAll(it) }
        notifyDataSetChanged()
    }
}