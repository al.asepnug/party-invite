package com.muxe.partyinvite.ui.allEvent

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPagerFragmentAdapter(fm: FragmentManager, lifecycle: Lifecycle, private var numberOfTabs: Int)
    : FragmentStateAdapter(fm, lifecycle) {

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                return EventAllFragment()
            }
            1 -> {
                return ForYouEventFragment()
            }
            else -> return EventAllFragment()
        }
    }

    override fun getItemCount(): Int {
        return numberOfTabs
    }
}