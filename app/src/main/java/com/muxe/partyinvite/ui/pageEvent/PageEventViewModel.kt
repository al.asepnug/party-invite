package com.muxe.partyinvite.ui.pageEvent

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.Messages
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PageEventViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _partyResponse = MutableLiveData<ApiResponse<Party>>()
    private val _messagesResponse = MutableLiveData<ApiResponse<List<Messages>>>()
    private val _messagesPreResponse = MutableLiveData<ApiResponse<List<Messages>>>()
    private val _likeResponse = MutableLiveData<ApiResponse<Message>>()
    private val _postMessagesResponse = MutableLiveData<ApiResponse<Message>>()

    val partyResponse = _partyResponse
    val messagesResponse = _messagesResponse
    val messagesPreResponse = _messagesPreResponse
    val likeResponse = _likeResponse
    val postMessagesResponse = _postMessagesResponse

    fun getPartyById(id: Int?) {
        viewModelScope.launch {
            dataRepository.getPartyById(id).collect{
                _partyResponse.value = it
            }
        }
    }

    fun getMessagesParty(typeMessage: String?, id: Int?) {
        viewModelScope.launch {
            dataRepository.getMessages(typeMessage, id).collect{
                _messagesResponse.value = it
            }
        }
    }

    fun getMessagesPreParty(typeMessage: String?, id: Int?) {
        viewModelScope.launch {
            dataRepository.getMessages(typeMessage, id).collect{
                _messagesPreResponse.value = it
            }
        }
    }

    fun postLikeMessages(like: String?, id: Int?) {
        viewModelScope.launch {
            dataRepository.postLikeMessages(like, id).collect{
                _likeResponse.value = it
            }
        }
    }

    fun postMessages(typeMessage: String?, id: Int?, message: String?) {
        viewModelScope.launch {
            dataRepository.postMessages(typeMessage, id, message).collect{
                _postMessagesResponse.value = it
            }
        }
    }
}