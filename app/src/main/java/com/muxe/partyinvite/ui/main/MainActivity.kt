package com.muxe.partyinvite.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.Group
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.button.MaterialButton
import com.google.android.material.navigation.NavigationView
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.ui.accountDetail.AccountDetailActivity
import com.muxe.partyinvite.ui.accountInfo.AccountInfoActivity
import com.muxe.partyinvite.ui.addParty.AddPartyActivity
import com.muxe.partyinvite.ui.login.LoginActivity
import com.muxe.partyinvite.utils.addOnClickListener
import com.muxe.partyinvite.utils.makeWarning
import com.muxe.partyinvite.utils.showImageRounded
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private val viewModel: MainViewModel by viewModels()
    private lateinit var tvUserName: TextView
    private lateinit var tvEmail: TextView
    private lateinit var imgUser: ImageView
    private lateinit var ratingBar: RatingBar
    private lateinit var btnPartyPlanner: MaterialButton
    private lateinit var groupProfile: Group
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextAppearance(this, R.style.TitleToolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        val headerView: View = navView.getHeaderView(0)
        tvUserName = headerView.findViewById(R.id.tvName)
        tvEmail = headerView.findViewById(R.id.tvEmail)
        imgUser = headerView.findViewById(R.id.imgProfile)
        ratingBar = headerView.findViewById(R.id.setRating)
        btnPartyPlanner = headerView.findViewById(R.id.btnPartyPlanner)
        btnPartyPlanner.setOnClickListener(this)
        groupProfile = headerView.findViewById(R.id.contentProfile)
        groupProfile.addOnClickListener{
            val intent = Intent(this, AccountDetailActivity::class.java)
            intent.putExtra(AccountDetailActivity.DATA_USER, user)
            startActivity(intent)
        }
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_all_event,
                R.id.nav_friend_event,
                R.id.nav_my_ticket,
                R.id.nav_party_followers,
                R.id.nav_account_settings,
                R.id.nav_frequently_asked
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val logOutItem: MenuItem = navView.menu.findItem(R.id.nav_logout)
        logOutItem.setOnMenuItemClickListener { //
            toast("logout")
            makeWarning(
                "Uitloggen Account ",
                "Weet je zeker dat je dit account uitlogt?",
                confirmListener = {
                    viewModel.deleteAllLocalData()
                    viewModel.removeAllSharedPref()
                    val intent = Intent(this, LoginActivity::class.java)
                    intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK or
                            Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                },
                cancelListener = {})
            true
        }

        populateUserData()
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    @SuppressLint("SetTextI18n")
    private fun populateUserData() {
        viewModel.getUser().observe(this, { result ->
            if (result != null) {
                user = result
                tvUserName.text = "${result.firstname} ${result.lastname}"
                result.avatar?.url?.let { showImageRounded(it, imgUser) }
                tvEmail.text = result.email
                ratingBar.rating = user.rating?.toFloat() ?: 0f
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnPartyPlanner -> {
                val intent = Intent(this, AddPartyActivity::class.java)
                startActivity(intent)
            }
        }
    }
}