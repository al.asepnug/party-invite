package com.muxe.partyinvite.ui.ticketInfo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Tickets
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TicketInfoViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _ticketResponse = MutableLiveData<ApiResponse<Tickets>>()

    val ticketResponse = _ticketResponse

    fun getTicketById(id: Int?) {
        viewModelScope.launch {
            dataRepository.getTicketById(id).collect{
                _ticketResponse.value = it
            }
        }
    }
}