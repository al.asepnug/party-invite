package com.muxe.partyinvite.ui.addParty

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.MessagesUpload
import com.muxe.partyinvite.data.entity.PartyCreated
import com.muxe.partyinvite.data.entity.Type
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class AddPartyViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _createdResponse = MutableLiveData<ApiResponse<PartyCreated>>()
    private val _partyTypeResponse = MutableLiveData<ApiResponse<List<Type>>>()
    private val _musicTypeResponse = MutableLiveData<ApiResponse<List<Type>>>()
    private val _uploadResponse = MutableLiveData<ApiResponse<MessagesUpload>>()

    val createdResponse = _createdResponse
    val partyTypeResponse = _partyTypeResponse
    val musicTypeResponse = _musicTypeResponse
    val uploadResponse = _uploadResponse

    fun postParty(fields: HashMap<String?, RequestBody?>) {
        viewModelScope.launch {
            dataRepository.postParty(fields).collect{
                _createdResponse.value = it
            }
        }
    }

    fun getPartyType() {
        viewModelScope.launch {
            dataRepository.getPartyType().collect{
                _partyTypeResponse.value = it
            }
        }
    }

    fun getMusicType() {
        viewModelScope.launch {
            dataRepository.getMusicType().collect{
                _musicTypeResponse.value = it
            }
        }
    }

    fun postUploadMedia(image: MultipartBody.Part, key: RequestBody) {
        viewModelScope.launch {
            dataRepository.postUploadMedia(image, key).collect{
                _uploadResponse.value = it
            }
        }
    }
}