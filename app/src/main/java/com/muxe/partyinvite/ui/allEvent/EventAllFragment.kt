package com.muxe.partyinvite.ui.allEvent

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.FragmentEventAllBinding
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.utils.*
import com.muxe.partyinvite.utils.DebouncingQueryTextListener
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class EventAllFragment : Fragment() {
    private val binding by lazy { FragmentEventAllBinding.inflate(layoutInflater) }
    private val viewModel: AllEventViewModel by viewModels()
    private val listParty = ArrayList<Party>()
    private var listNearbyParty: List<Party>? = null
    private lateinit var pDialog: SweetAlertDialog
    private var latitude: String? = null
    private var longitude: String? = null

    companion object {
        fun newInstance() = EventAllFragment()
        const val PATH_TYPE = ""
        const val DATA_PARTY = "data_party"
        fun newInstance(data: ArrayList<Party>): EventAllFragment {
            val fragment = EventAllFragment()

            val bundle = Bundle().apply {
                putParcelableArrayList(DATA_PARTY, data)
//                putString("latitude", latitude)
//                putString("longitude", longitude)
            }

            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        pDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.PROGRESS_TYPE)
        initUi()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateData()
        try {
            if (arguments?.containsKey(DATA_PARTY) == true) {
                listNearbyParty = arguments?.getParcelableArrayList(DATA_PARTY)
                //latitude = arguments?.getString("latitude")
                // longitude = arguments?.getString("longitude")
                updateList()
            } else {
                viewModel.getParty(PATH_TYPE)
            }
        } catch (e: IOException) {
            requireActivity().toast("Error Load Data")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val listItem: MenuItem? = menu.findItem(R.id.action_list)
        listItem?.isVisible = false
        val mapsItem: MenuItem? = menu.findItem(R.id.action_maps)
        mapsItem?.isVisible = true

        val searchItem: MenuItem? = menu.findItem(R.id.action_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as androidx.appcompat.widget.SearchView
            searchView.setOnCloseListener { true }
            val searchPlate =
                searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
            searchPlate.hint = "Zoeken Alle Festen"
            val searchPlateView: View =
                searchView.findViewById(androidx.appcompat.R.id.search_plate)
            searchPlateView.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.transparent
                )
            )

            searchView.setOnQueryTextListener(DebouncingQueryTextListener(
                requireActivity()
            ) { newText ->
                newText?.let {
                    if (it.isNotEmpty()) {
                        viewModel.getPartySearch(it, "period")
                    }
                }
            })

            searchView.setOnCloseListener {
                if (listNearbyParty != null) {
                    viewModel.getPartyNearby(latitude, longitude)
                } else {
                    viewModel.getParty(PATH_TYPE)
                }
                searchView.onActionViewCollapsed()
                false
            }
            val searchManager =
                requireActivity().getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))

        }
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_maps -> {
                val bundle = Bundle()
                bundle.putParcelableArrayList(DATA_PARTY, listParty)
                binding.fragmentContainer.visibility = View.VISIBLE
                childFragmentManager.commit {
                    setReorderingAllowed(true)
                    addToBackStack(null)
                    replace<ForYouEventFragment>(R.id.fragment_container, args = bundle)
                }
                true
            }
            R.id.action_list -> {
                val bundle = Bundle()
                bundle.putParcelableArrayList(DATA_PARTY, listParty)
                binding.fragmentContainer.visibility = View.GONE
                childFragmentManager.popBackStack()
//                childFragmentManager.commit {
//                    setReorderingAllowed(false)
//                    replace<EventAllFragment>(R.id.fragment_container, args = bundle)
//                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initUi() {
        binding.rvEvent.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = EventAdapter(viewModel.getLocation(), context, listParty, clickListener = {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, it)
                context.startActivity(intent)
            }, clickFav = {
                viewModel.getPartyFavorite(it)
            })
        }
    }

    private fun updateList() {
        binding.rvEvent.adapter?.let { adapter ->
            when (adapter) {
                is EventAdapter -> {
                    try {
                        if (listNearbyParty != null) {
                            Log.d("updatelist", listNearbyParty!!.size.toString())
                            adapter.updateData(listNearbyParty!!)
                        } else {
                            requireActivity().toast("Data is Empty")
                        }
                    } catch (e: IOException) {
                        requireActivity().toast("Error Load Data")
                    }
                }
            }
        }
    }

    private fun populateData() {
        viewModel.partyFavoriteResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.body?.let {
                        requireActivity().makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            if (listNearbyParty != null) {
                                viewModel.getPartyNearby(latitude, longitude)
                            } else {
                                viewModel.getParty(PATH_TYPE)
                            }
                        })

                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    requireActivity().makeLoading(pDialog, true)
                }

            }
        })

        viewModel.partyResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvEvent.adapter?.let { adapter ->
                            when (adapter) {
                                is EventAdapter -> {
                                    try {
                                        if (it.isNotEmpty()) {
                                            listParty.clear()
                                            listParty.addAll(it)
                                            adapter.updateData(it)
                                        } else {
                                            requireActivity().toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        requireActivity().toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }
}