package com.muxe.partyinvite.ui.myTicket

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.muxe.partyinvite.data.entity.Tickets
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.MyTicketFragmentBinding
import com.muxe.partyinvite.ui.ticketDetails.TicketDetailsActivity
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class MyTicketFragment : Fragment() {
    private val binding by lazy { MyTicketFragmentBinding.inflate(layoutInflater) }
    private val viewModel: MyTicketViewModel by viewModels()
    private val listTickets = ArrayList<Tickets>()

    companion object {
        fun newInstance() = MyTicketFragment()
        const val DATA_TICKETS = "data_tickets"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initUi()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getTickets()
    }

    private fun initUi() {
        binding.rvTicket.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = MyTicketAdapter(context, listTickets, clickListener = {
                val intent = Intent(context, TicketDetailsActivity::class.java)
                intent.putExtra(DATA_TICKETS, it)
                context.startActivity(intent)
            })
        }
    }

    private fun populateData() {
        viewModel.ticketsResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvTicket.adapter?.let { adapter ->
                            when (adapter) {
                                is MyTicketAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            requireActivity().toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        requireActivity().toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }

}