package com.muxe.partyinvite.ui.login

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import cn.pedant.SweetAlert.SweetAlertDialog
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityLoginBinding
import com.muxe.partyinvite.ui.main.MainActivity
import com.muxe.partyinvite.ui.register.RegisterActivity
import com.muxe.partyinvite.utils.makeLoading
import com.muxe.partyinvite.utils.makeStatusBarTransparent
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityLoginBinding
    private val viewModel: LoginViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    var callbackManager: CallbackManager? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val RC_SIGN_IN = 1
    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        try {
            makeStatusBarTransparent()
            callbackManager = CallbackManager.Factory.create()
            binding.facebookLogin.setReadPermissions(listOf("email", "public_profile"))
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        } catch (e: Exception) {
            toast("makeStatusBarTransparent: $e")
        }
        // init on click
        binding.btnLogin.setOnClickListener(this)
        binding.btnRegister.setOnClickListener(this)
        binding.btnFacebook.setOnClickListener(this)
        binding.btnGoogle.setOnClickListener(this)
        //init dialog
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        subscribeLoginResponse()
        subscribeUserResponse()
        subscribeFacebookResponse()
        subscribeRegisterResponse()
        try {
            binding.facebookLogin.registerCallback(
                callbackManager,
                object : FacebookCallback<LoginResult?> {
                    override fun onSuccess(loginResult: LoginResult?) {
                        // App code
                        loginResult?.accessToken?.let {
                            viewModel.getFacebookData(it.userId, it.token)
                        }
                    }

                    override fun onCancel() {
                        Log.d("onCancel", "sf")
                        toast("Sign in facebook canceled")
                    }

                    override fun onError(exception: FacebookException) {
                        Log.d("onErrorFb", exception.toString())
                        toast("Sign in facebook error")
                    }
                })
        } catch (e: Exception) {
            toast("registerCallback: $e")
        }

//        printHashKey()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
            if (requestCode == RC_SIGN_IN) {
                val task =
                    GoogleSignIn.getSignedInAccountFromIntent(data)

                try {
                    val account: GoogleSignInAccount? = task.getResult(ApiException::class.java)
                    account?.let { it ->
                        val field: HashMap<String?, RequestBody?> = HashMap()
                        field["firstname"] =
                            (it.givenName)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["lastname"] =
                            (it.familyName)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["email"] = (it.email)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["social_google_id"] =
                            (it.id)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        if (it.photoUrl != null) {
                            field["avatar_url"] =
                                (it.photoUrl.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
                        } else {
                            field["avatar_url"] =
                                ("https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png")
                                    .toRequestBody("text/plain".toMediaTypeOrNull())
                        }

                        viewModel.postRegister(field)
                    }

                } catch (e: ApiException) {
                    // The ApiException status code indicates the detailed failure reason.
                    // Please refer to the GoogleSignInStatusCodes class reference for more information.
                    Log.e("TAG", "signInResult:failed code=" + e.statusCode)
                    toast("Sign in google error")
                }
            }
            super.onActivityResult(requestCode, resultCode, data)
        } catch (e: Exception) {
            toast("onActivityResult: $e")
        }
    }

//    fun printHashKey() {
//
//        // Add code to print out the key hash
//        try {
//            val info = packageManager.getPackageInfo(
//                "com.muxe.partyinvite",
//                PackageManager.GET_SIGNATURES
//            )
//            for (signature in info.signatures) {
//                val md: MessageDigest = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                Log.d(
//                    "KeyHash:",
//                    Base64.encodeToString(
//                        md.digest(),
//                        Base64.DEFAULT
//                    )
//                )
//            }
//        } catch (e: PackageManager.NameNotFoundException) {
//            Log.d("keyhasherrorpa", e.toString())
//        } catch (e: NoSuchAlgorithmException) {
//            Log.d("keyhasherror", e.toString())
//        }
//    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnLogin -> {
                if (binding.edtEmail.text.toString()
                        .isEmpty() || binding.edtPassword.text.toString().isEmpty()
                ) {
                    toast("Form is empty")
                } else {
                    val body = JSONObject()
                    body.put("username", binding.edtEmail.text.toString())
                    body.put("password", binding.edtPassword.text.toString())
                    viewModel.postLogin(body.toString())
                }
            }

            R.id.btnRegister -> {
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }

            R.id.btnFacebook -> {
                binding.facebookLogin.performClick()
            }

            R.id.btnGoogle -> {
                val intent = mGoogleSignInClient!!.signInIntent
                startActivityForResult(intent, RC_SIGN_IN)
            }
        }
    }

    private fun subscribeRegisterResponse() {
        viewModel.registerResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (result.body?.status.equals("social_login_ok")
                        || result.body?.status.equals("registration_complete")
                    ) {
                        viewModel.getUser()
                    } else {
                        result.body?.error?.let { toast(it) }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }

    private fun subscribeFacebookResponse() {
        viewModel.facebookResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        val field: HashMap<String?, RequestBody?> = HashMap()
                        field["email"] = (it.email)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["firstname"] =
                            (it.firstName)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["lastname"] =
                            (it.lastName)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["social_fb_id"] =
                            (it.id)?.toRequestBody("text/plain".toMediaTypeOrNull())
                        field["avatar_url"] =
                            (it.picture?.data?.url)?.toRequestBody("text/plain".toMediaTypeOrNull())

                        viewModel.postRegister(field)
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }

    private fun subscribeLoginResponse() {
        viewModel.loginResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.token.let {
                        viewModel.getUser()
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }

    private fun subscribeUserResponse() {
        viewModel.userResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    if (result.body != null) {
                        result.body.let {
                            toast("Successfully Login")
                            handler = Handler()
                            handler.postDelayed({
                                val intent = Intent(this, MainActivity::class.java)
                                intent.addFlags(
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                            Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                            Intent.FLAG_ACTIVITY_NEW_TASK
                                )
                                startActivity(intent)
                            }, 200)
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }
}