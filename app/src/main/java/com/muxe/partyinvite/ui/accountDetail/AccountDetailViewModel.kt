package com.muxe.partyinvite.ui.accountDetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.MessagesUpload
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class AccountDetailViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _userResponse = MutableLiveData<ApiResponse<UserDetails>>()
    private val _messageResponse = MutableLiveData<ApiResponse<Message>>()
    private val _uploadResponse = MutableLiveData<ApiResponse<MessagesUpload>>()

    val userResponse = _userResponse
    val messageResponse = _messageResponse
    val uploadResponse = _uploadResponse

    fun getUser(idUser: Int) {
        viewModelScope.launch {
            dataRepository.getFullProfile(idUser).collect{
                _userResponse.value = it
            }
        }
    }

    fun putUser(body: String) {
        viewModelScope.launch {
            dataRepository.putProfile(body).collect{
                _messageResponse.value = it
            }
        }
    }

    fun postUploadMedia(image: MultipartBody.Part, key: RequestBody) {
        viewModelScope.launch {
            dataRepository.postUploadMedia(image, key).collect{
                _uploadResponse.value = it
            }
        }
    }

    fun inserUser(user: User) = dataRepository.insertUser(user)
}