package com.muxe.partyinvite.ui.reviewsList

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Reviews
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityReviewsBinding
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class ReviewsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityReviewsBinding
    private val viewModel: ReviewsViewModel by viewModels()
    private val listReviews = ArrayList<Reviews>()
    private var followingDetails: UserDetails? = null

    companion object {
        const val DATA_RATING = "data_rating"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReviewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.title = "Reviews"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        try {
            followingDetails = intent.extras?.getParcelable(DATA_RATING)
            viewModel.getReviews(followingDetails?.id)
            initUi()
            populateData()
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun initUi() {
        binding.tvUser.text = "${followingDetails?.firstname} ${followingDetails?.lastname}"
        binding.setRating.rating = followingDetails?.rating?.toFloat() ?: 0F
        binding.tvRating.text = "${followingDetails?.rating?.toInt() ?: "0"}/5"
        binding.rvReviews.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = ReviewsAdapter(listReviews)
        }
    }

    private fun populateData() {
        viewModel.reviewsResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvReviews.adapter?.let { adapter ->
                            when (adapter) {
                                is ReviewsAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }
}