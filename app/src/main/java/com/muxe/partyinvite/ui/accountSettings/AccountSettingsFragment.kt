package com.muxe.partyinvite.ui.accountSettings

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.format.Formatter.formatShortFileSize
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.BuildConfig
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.databinding.FragmentAccountSettingsBinding
import com.muxe.partyinvite.ui.accountDetail.AccountDetailActivity
import com.muxe.partyinvite.ui.login.LoginActivity
import com.muxe.partyinvite.ui.termsAndCondition.TermsActivity
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.File

@AndroidEntryPoint
class AccountSettingsFragment : Fragment(), View.OnClickListener {
    private val binding by lazy { FragmentAccountSettingsBinding.inflate(layoutInflater) }
    private val viewModel: AccountSettingsViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private lateinit var user: User

    companion object {
        fun newInstance() = AccountSettingsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        pDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.PROGRESS_TYPE)

        // Inflate the layout for this fragment
        binding.tvClear.setOnClickListener(this)
        binding.contentProfile.addOnClickListener{
            val intent = Intent(context, AccountDetailActivity::class.java)
            intent.putExtra(AccountDetailActivity.DATA_USER, user)
            context?.startActivity(intent)
        }
        binding.tvPolicies.setOnClickListener(this)
        binding.tvLogout.setOnClickListener(this)
        viewModel.getUser()
        populateData()
        initUi()
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun updateUi() {
        user.avatar?.url?.let { requireActivity().showImageRounded(it, binding.imgProfile) }
        binding.tvName.text = "${user.firstname} ${user.lastname}"
        binding.tvEmail.text = user.email
        binding.setRating.rating = user.rating?.toFloat() ?: 0f
    }

    private fun initUi() {
        binding.tvAppversionVal.text = BuildConfig.VERSION_NAME
        binding.tvCacheVal.text =
            context?.cacheDir?.calculateSizeRecursively()?.let { formatShortFileSize(context, it)}
    }

    private fun populateData() {
        viewModel.getUser().observe(viewLifecycleOwner, { result ->
            if (result != null) {
                user = result
                updateUi()
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.tvClear -> {
                context?.let { deleteCache(it) }
            }

            R.id.tvPolicies -> {
                val intent = Intent(context, TermsActivity::class.java)
                context?.startActivity(intent)
            }

            R.id.tvLogout -> {
                requireActivity().makeWarning(
                    "Uitloggen Account ",
                    "Weet je zeker dat je dit account uitlogt?",
                    confirmListener = {
                        viewModel.deleteAllLocalData()
                        viewModel.removeAllSharedPref()
                        val intent = Intent(context, LoginActivity::class.java)
                        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP or
                                Intent.FLAG_ACTIVITY_CLEAR_TASK or
                                Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    },
                    cancelListener = {})
            }
        }
    }

    @SuppressLint("SetTextI18n")
    fun deleteCache(context: Context) {
        try {
            val dir: File = context.cacheDir
            deleteDir(dir)
            binding.tvCacheVal.text = context.cacheDir?.calculateSizeRecursively()?.let { formatShortFileSize(context, it)}
        } catch (e: Exception) {
        }
    }

    private fun deleteDir(dir: File?): Boolean {
        return if (dir != null && dir.isDirectory) {
            val children: Array<String> = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
            dir.delete()
        } else if (dir != null && dir.isFile) {
            dir.delete()
        } else {
            false
        }
    }
}