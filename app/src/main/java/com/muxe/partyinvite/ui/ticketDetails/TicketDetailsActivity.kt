package com.muxe.partyinvite.ui.ticketDetails

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Tickets
import com.muxe.partyinvite.databinding.ActivityTicketDetailsBinding
import com.muxe.partyinvite.ui.myTicket.MyTicketFragment.Companion.DATA_TICKETS
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.utils.ValueFormat
import com.muxe.partyinvite.utils.showImageCenterCrop
import com.muxe.partyinvite.utils.showImageCenterInside
import com.muxe.partyinvite.utils.toast
import java.io.IOException

class TicketDetailsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityTicketDetailsBinding
    private lateinit var tickets: Tickets

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTicketDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.layProceed.setOnClickListener(this)
        binding.tvTitle.setOnClickListener(this)
        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        try {
            tickets = intent.extras?.getParcelable(DATA_TICKETS)!!
            initUi()
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun initUi() {
        binding.tvTitle.text = tickets.party?.label
        binding.tvPlace.text = "${tickets.party?.address}, ${tickets.party?.postalcode}, ${tickets.party?.city}"
        binding.tvDate.text = ValueFormat.formatDate(tickets.date)
        binding.tvTime.text = ValueFormat.formatTime(tickets.date)
        binding.tvVisitor.text = "${tickets.user?.firstname} ${tickets.user?.lastname}"
        tickets.party?.cover?.url?.let { showImageCenterCrop(it, binding.imgContent) }
        tickets.qr?.let { showImageCenterInside("https://party.qinox.nl$it", binding.imgBarcode) }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.layProceed -> {
                try {
//                    val gmmIntentUri =
//                        Uri.parse("google.navigation:q=${tickets.party?.latitude},${tickets.party?.longitude}")
//                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//                    mapIntent.setPackage("com.google.android.apps.maps")
//                    startActivity(mapIntent)

//                    val gmmIntentUri =
//                        Uri.parse("http://maps.google.com/maps?q=${tickets.party?.latitude},${tickets.party?.longitude}")
//                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//                    startActivity(mapIntent)
                    val gmmIntentUri = Uri.parse("geo:0,0?q=${tickets.party?.latitude},${tickets.party?.longitude}")
                        //Uri.parse("http://maps.google.com/maps?q=${tickets.party?.latitude},${tickets.party?.longitude}")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    startActivity(mapIntent)
                } catch (e: IOException) {
                    toast("Error Load Google Mpas")
                }
            }

            R.id.tvTitle -> {
                val intent = Intent(this, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, tickets.party)
                startActivity(intent)
            }
        }
    }
}