package com.muxe.partyinvite.ui.friendEvent

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.FriendEventFragmentBinding
import com.muxe.partyinvite.ui.allEvent.EventAdapter
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.utils.makeLoading
import com.muxe.partyinvite.utils.makeSuccess
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class FriendEventFragment : Fragment() {
    private val binding by lazy { FriendEventFragmentBinding.inflate(layoutInflater) }
    private val viewModel: FriendEventViewModel by viewModels()
    private val listParty = ArrayList<Party>()
    private lateinit var pDialog: SweetAlertDialog

    companion object {
        fun newInstance() = FriendEventFragment()
        const val PATH_TYPE = "followers"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        pDialog = SweetAlertDialog(requireContext(), SweetAlertDialog.PROGRESS_TYPE)
        initUi()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateData()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getParty(PATH_TYPE)
    }

    private fun initUi() {
        binding.rvEvent.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = EventAdapter(viewModel.getLocation(), context, listParty, clickListener = {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, it)
                context.startActivity(intent)
            }, clickFav = {
                viewModel.getPartyFavorite(it)
            })
        }
    }

    private fun populateData() {
        viewModel.partyFavoriteResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.body?.let {
                        requireActivity().makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            viewModel.getParty(PATH_TYPE)
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    requireActivity().makeLoading(pDialog, false)
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    requireActivity().makeLoading(pDialog, true)
                }

            }
        })

        viewModel.partyResponse.observe(viewLifecycleOwner, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        binding.rvEvent.adapter?.let { adapter ->
                            when (adapter) {
                                is EventAdapter -> {
                                    try {
                                        if (it.size > 0) {
                                            adapter.updateData(it)
                                        } else {
                                            requireActivity().toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        requireActivity().toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { requireActivity().toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }
}