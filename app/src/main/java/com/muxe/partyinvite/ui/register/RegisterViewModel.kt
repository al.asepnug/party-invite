package com.muxe.partyinvite.ui.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.FacebookResponse
import com.muxe.partyinvite.data.entity.Login
import com.muxe.partyinvite.data.entity.Register
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
        private val dataRepository: DataRepository
) : ViewModel() {
    private val _registerResponse = MutableLiveData<ApiResponse<Register?>>()
    private val _userResponse = MutableLiveData<ApiResponse<User>>()
    private val _facebookResponse = MutableLiveData<ApiResponse<FacebookResponse?>>()

    val registerResponse = _registerResponse
    val userResponse = _userResponse
    val facebookResponse = _facebookResponse

    fun postRegister(fields: HashMap<String?, RequestBody?>) {
        viewModelScope.launch {
            dataRepository.postRegister(fields).collect{
                _registerResponse.value = it
            }
        }
    }

    fun getUser() {
        viewModelScope.launch {
            dataRepository.getProfile().collect{
                _userResponse.value = it
            }
        }
    }

    fun getFacebookData(userId: String, authHeader: String) {
        viewModelScope.launch {
            dataRepository.getFacebookData(userId, authHeader).collect{
                _facebookResponse.value = it
            }
        }
    }
}