package com.muxe.partyinvite.ui.confirmTickets

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ConfirmTicketsViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _joinPartyResponse = MutableLiveData<ApiResponse<Message>>()

    val joinPartyResponse = _joinPartyResponse

    fun getJoinParty(id: Int?) {
        viewModelScope.launch {
            dataRepository.getJoinParty(id).collect{
                _joinPartyResponse.value = it
            }
        }
    }
}