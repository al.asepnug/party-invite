package com.muxe.partyinvite.ui.confirmTickets

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityConfirmTicketsBinding
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.ui.ticketInfo.TicketInfoActivity
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class ConfirmTicketsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityConfirmTicketsBinding
    private lateinit var party: Party
    private val viewModel: ConfirmTicketsViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog

    companion object {
        const val DATA = "id_ticket"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmTicketsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.layProceed.setOnClickListener(this)
        //init dialog
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.title = "Bevestig jouw ticket boeking"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        try {
            party = intent.extras?.getParcelable(PageEvent.DATA)!!
            initUi()
            subscribeDataResponse()
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun initUi() {
        binding.tvTitle.text = party.label
        binding.tvPlace.text = "${party.address}, ${party.postalcode}, ${party.city}"
        binding.tvDistance.text = "${party.distance} km afstand"
        binding.tvDate.text = ValueFormat.formatDateTimed(party.date)
        binding.tvTicketPrice.text = "€ ${party.price} per stuk"
        binding.tvTicketAmount.text = " | 1 Tickets voor "
        binding.tvTicketTotalPrice.text = "€ ${party.price}"
        val administrative = (2.5 * party.price!!) /100
        binding.tvAdminitratieveKostenPrice.text = "€ $administrative"
        binding.tvPrice.text = "€ ${(administrative + party.price!!)}"
        party.cover?.url?.let { showImageRounded(it, binding.imgContent) }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.layProceed -> {
                viewModel.getJoinParty(party.id)
            }
            R.id.btnBack -> {
                onBackPressed()
            }
        }
    }

    private fun subscribeDataResponse() {
        viewModel.joinPartyResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (result.body?.message != null) {
                        result.body.message.let {
                            if (it.equals("party_joined")) {
                                makeSuccess(title = "Success", content = "Successfully " +
                                        it.replace('_', ' '),
                                    confirmListener = {
                                        val intent = Intent(this, TicketInfoActivity::class.java)
                                        intent.putExtra(DATA, result.body.id)
                                        startActivity(intent)
                                        finish()
                                    })
                            } else {
                                toast(it.replace('_', ' '))
                            }
                        }
                    }

                    if (result.body?.error !=null) {
                        toast(result.body.error.replace('_', ' '))
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }
}