package com.muxe.partyinvite.ui.splash

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationServices
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.muxe.partyinvite.data.entity.Location
import com.muxe.partyinvite.databinding.ActivitySplashBinding
import com.muxe.partyinvite.ui.main.MainActivity
import com.muxe.partyinvite.ui.login.LoginActivity
import com.muxe.partyinvite.utils.makeStatusBarTransparent
import com.muxe.partyinvite.utils.makeWarning
import com.muxe.partyinvite.utils.toast
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {
    private val viewModel: SplashViewModel by viewModels()
    private lateinit var binding: ActivitySplashBinding
    lateinit var handler: Handler
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        try {
            makeStatusBarTransparent()
            handler = Handler()
            handler.postDelayed({
                if (checkPlayServices()) {
                    askForPermissions()
                }
            }, 200)
        } catch (e: Exception) {
            toast("statusbar: $e")
        }
    }

    private fun checkIsLogin() {
        try {
            if (viewModel.getIsLogin()) {
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        } catch (e: Exception) {
            toast(e.toString())
        }

    }

    override fun onPause() {
        super.onPause()
        if (::fusedLocationClient.isInitialized && ::locationCallback.isInitialized) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    private fun askForPermissions() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            if (locationEnabled()) {
                                loadLocation()
                            } else {
                                makeWarning(title = "GPS moet zijn ingeschakeld",
                                    content = "GPS inschakelen via instellingen?",
                                    confirmListener = {
                                        val intent =
                                            Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                                        startActivity(intent)
                                        finish()
                                    },
                                    cancelListener = {
                                        finish()
                                    })
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            makeWarning(title = "Locatievergunning vereist",
                                content = "Gebruik van locatie toestaan via instellingen?",
                                confirmListener = {
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts(
                                        "package",
                                        packageName,
                                        null
                                    )
                                    intent.data = uri
                                    startActivity(intent)
                                    finish()
                                },
                                cancelListener = {
                                    finish()
                                })
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    // Remember to invoke this method when the custom rationale is closed
                    // or just by default if you don't want to use any custom rationale.
                    token?.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Log.d("Splash", it.name)
            }
            .check()
    }

    private fun loadLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        // Add a marker and move the camera

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                viewModel.setLocation(Location(location.latitude.toString(), location.longitude.toString()))
                checkIsLogin()
            } else {
                toast("exacte locatie niet gevonden")
                checkIsLogin()
            }
        }
    }

    private fun locationEnabled(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                makeWarning(title = "Vereist Google Play-services",
                    content = "Download Google Play-services in de Play Store",
                    confirmListener = {
                        val intent = Intent(Intent.ACTION_VIEW).apply {
                            data = Uri.parse(
                                "https://play.google.com/store/apps/details?id=com.example.android"
                            )
                            setPackage("com.android.vending")
                        }
                        startActivity(intent)
                        finish()
                    },
                    cancelListener = {
                        finish()
                    })
            } else {
                toast("Dit apparaat is niet beschikbaar Google Play-service")
                finish()
            }
            return false
        }
        return true
    }
}