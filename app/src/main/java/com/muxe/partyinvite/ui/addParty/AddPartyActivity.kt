package com.muxe.partyinvite.ui.addParty

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationServices
import com.google.android.material.button.MaterialButton
import com.google.android.material.datepicker.MaterialDatePicker
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.muxe.partyinvite.BuildConfig
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Type
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityAddPartyBinding
import com.muxe.partyinvite.utils.*
import com.muxe.partyinvite.utils.ValueFormat.convertLongToTime
import com.muxe.partyinvite.utils.ValueFormat.formatDateNow
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

@AndroidEntryPoint
class AddPartyActivity : AppCompatActivity(), View.OnClickListener,
    UploadRequestBody.UploadCallback {
    private lateinit var binding: ActivityAddPartyBinding
    private val viewModel: AddPartyViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private lateinit var adapterPartyType: ArrayAdapter<Type>
    private var idPartyType: Int = 0
    private lateinit var adapterMusicType: ArrayAdapter<Type>
    private var idMusicType: Int = 0
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var latitude: String? = null
    private var longitude: String? = null
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_PICK_IMAGE = 2
    private var currentPhotoPath: String? = null
    private var uriImage: Uri? = null
    private lateinit var dialog: Dialog
    private var isCamera: Boolean = false
    private lateinit var potoImage: File

    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddPartyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appbar.toolbar)
        supportActionBar?.title = "Party Planner"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.appbar.toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_arrow_back_ios_24)
        binding.appbar.toolbar.navigationIcon?.setTint(ContextCompat.getColor(this, R.color.white))
        binding.btnSave.setOnClickListener(this)
        binding.imgEvent.setOnClickListener(this)
        binding.spinnerFeestType.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    // You can define you actions as you want
                }

                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    val selectedObject = binding.spinnerFeestType.selectedItem as Type
                    idPartyType = selectedObject.id
                }
            }
        binding.spinnerMusicType.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    // You can define you actions as you want
                }

                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    position: Int,
                    p3: Long
                ) {
                    val selectedObject = binding.spinnerMusicType.selectedItem as Type
                    idMusicType = selectedObject.id
                }
            }

        binding.checkLocation.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                if (checkPlayServices()) {
                    askForPermissions()
                }
            }
        }

        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText("Kies Feestdatum ")
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
            .build()
        datePicker.addOnNegativeButtonClickListener { datePicker.dismiss() }
        datePicker.addOnPositiveButtonClickListener {
            binding.edtDate.setText(convertLongToTime(it))
            datePicker.dismiss()
        }

        binding.edtDate.setOnClickListener {
            datePicker.show(supportFragmentManager, datePicker.toString())
        }

        binding.edtDate.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                datePicker.show(supportFragmentManager, datePicker.toString())
            } else {
                datePicker.dismiss()
            }
        }
        val cal = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
            cal.set(Calendar.HOUR_OF_DAY, hour)
            cal.set(Calendar.MINUTE, minute)
            cal.set(Calendar.SECOND, 0)
            binding.edtTime.setText(SimpleDateFormat("HH:mm:ss").format(cal.time))
        }
        val timePicker = TimePickerDialog(
            this,
            timeSetListener,
            cal.get(Calendar.HOUR_OF_DAY),
            cal.get(Calendar.MINUTE),
            true
        )
        binding.edtTime.setOnClickListener {
            timePicker.show()
        }
        binding.edtTime.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                timePicker.show()
            } else {
                timePicker.dismiss()
            }
        }
        //init dialog
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        viewModel.getPartyType()
        viewModel.getMusicType()
        subscribeDataResponse()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun resetUi() {
        binding.edtLabel.text?.clear()
        binding.edtPrice.text?.clear()
        binding.edtAddress.text?.clear()
        binding.edtPostcode.text?.clear()
        binding.edtCity.text?.clear()
        binding.edtLocationType.text?.clear()
        binding.edtMaxParticipant.text?.clear()
        binding.edtTime.text?.clear()
        binding.edtDate.text?.clear()
        binding.edtMinAge.text?.clear()
        binding.edtMaxAge.text?.clear()
        binding.edtDescriptionVal.text?.clear()
        binding.imgEvent.setImageResource(R.drawable.bg_party_planner)
        binding.checkQrCode.isChecked = false
        binding.checkLocation.isChecked = false
        binding.checkVisibility.isChecked = false
    }

    private fun subscribeDataResponse() {
        viewModel.uploadResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let { it ->
                        if (it.error.equals("")) {
                            postParty(it.id)
                        } else {
                            it.error?.let { it1 -> toast(it1) }
                        }
                    }

                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {

                }
            }
        })

        viewModel.createdResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.message.let {
                        if (it.equals("party_created")) {
                            makeSuccess(title = "Success", content = "Successfully " +
                                    "${it?.replace('_', ' ')}",
                                confirmListener = {
                                    resetUi()
                                })
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })

        viewModel.partyTypeResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (result.body != null && result.body.isNotEmpty()) {
                        adapterPartyType =
                            ArrayAdapter(this, android.R.layout.simple_list_item_1, result.body)
                        binding.spinnerFeestType.setSelection(0)
                        binding.spinnerFeestType.adapter = adapterPartyType
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.musicTypeResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    if (result.body != null && result.body.isNotEmpty()) {
                        adapterMusicType =
                            ArrayAdapter(this, android.R.layout.simple_list_item_1, result.body)
                        binding.spinnerMusicType.setSelection(0)
                        binding.spinnerMusicType.adapter = adapterMusicType
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imgEvent -> {
                showDialogEdtPhoto()
            }

            R.id.btnSave -> {
                if (binding.edtLabel.text.toString().isEmpty() || binding.edtAddress.text.toString()
                        .isEmpty() || binding.edtPostcode.text.toString().isEmpty()
                    || binding.edtPrice.text.toString()
                        .isEmpty() || binding.edtMinAge.text.toString()
                        .isEmpty() || binding.edtMaxAge.text.toString().isEmpty()
                ) {
                    toast("Form is empty")
                } else {
                    if (uriImage != null) {
                        uploadPicture()
                    } else {
                        postParty(null)
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (::fusedLocationClient.isInitialized && ::locationCallback.isInitialized) {
            fusedLocationClient.removeLocationUpdates(locationCallback)
        }
    }

    private fun postParty(cover_id: Int?) {
        val field: HashMap<String?, RequestBody?> = HashMap()
        if (cover_id != null) {
            field["cover_id"] = (cover_id.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        }
        field["label"] = (binding.edtLabel.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["description"] = (binding.edtDescriptionVal.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["address"] = (binding.edtAddress.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["postalcode"] = (binding.edtPostcode.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["city"] = (binding.edtCity.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["price"] = (binding.edtPrice.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
       // field["location_type"] = (binding.edtLocationType.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["max_guests"] = (binding.edtMaxParticipant.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["public"] = ((R.id.rbOpen == binding.rbAvaibility.checkedRadioButtonId).toString())
            .toRequestBody("text/plain".toMediaTypeOrNull())
        field["gender"] = ((when (R.id.rbCouple) {binding.rbGender.checkedRadioButtonId -> 0
            binding.rbGender.checkedRadioButtonId -> 1 else -> 2 }).toString())
            .toRequestBody("text/plain".toMediaTypeOrNull())
        field["type"] = (idPartyType.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["date"] = ("${binding.edtDate.text} ${binding.edtTime.text}").toRequestBody("text/plain".toMediaTypeOrNull())
       // field["date_add"] = (formatDateNow()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["age_from"] = (binding.edtMinAge.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["age_to"] = (binding.edtMaxAge.text.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["type_music"] = (idMusicType.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["require_qr"] = (binding.checkQrCode.isChecked.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["show_exact_location"] = (binding.checkLocation.isChecked.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        if (binding.checkLocation.isChecked ) {
            if (latitude != null && longitude != null) {
                field["latitude"] = (latitude)?.toRequestBody("text/plain".toMediaTypeOrNull())
                field["longitude"] = (longitude)?.toRequestBody("text/plain".toMediaTypeOrNull())
            }
        }
        field["show_joined"] = (binding.checkVisibility.isChecked.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        field["enabled"] = (true.toString()).toRequestBody("text/plain".toMediaTypeOrNull())
        viewModel.postParty(field)
    }

    private fun askForPermissions() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            if (locationEnabled()) {
                                loadLocation()
                            } else {
                                makeWarning(title = "GPS moet zijn ingeschakeld",
                                    content = "GPS inschakelen via instellingen?",
                                    confirmListener = {
                                        val intent =
                                            Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                                        startActivity(intent)
                                        finish()
                                    },
                                    cancelListener = {
                                        finish()
                                    })
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            makeWarning(title = "Locatievergunning vereist",
                                content = "Gebruik van locatie toestaan via instellingen?",
                                confirmListener = {
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts(
                                        "package",
                                        packageName,
                                        null
                                    )
                                    intent.data = uri
                                    startActivity(intent)
                                    finish()
                                },
                                cancelListener = {
                                    finish()
                                })
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    // Remember to invoke this method when the custom rationale is closed
                    // or just by default if you don't want to use any custom rationale.
                    token?.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Log.d("AddPartyActivity", it.name)
            }
            .check()
    }

    private fun loadLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        // Add a marker and move the camera

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                latitude = location.latitude.toString()
                longitude = location.longitude.toString()
            } else {
                toast("exacte locatie niet gevonden")
            }
        }
    }

    private fun locationEnabled(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun checkPlayServices(): Boolean {
        val apiAvailability = GoogleApiAvailability.getInstance()
        val resultCode = apiAvailability.isGooglePlayServicesAvailable(this)
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                makeWarning(title = "Vereist Google Play-services",
                    content = "Download Google Play-services in de Play Store",
                    confirmListener = {
                        val intent = Intent(Intent.ACTION_VIEW).apply {
                            data = Uri.parse(
                                "https://play.google.com/store/apps/details?id=com.example.android"
                            )
                            setPackage("com.android.vending")
                        }
                        startActivity(intent)
                        finish()
                    },
                    cancelListener = {
                        finish()
                    })
            } else {
                toast("Dit apparaat is niet beschikbaar Google Play-service")
                finish()
            }
            return false
        }
        return true
    }


    private fun showDialogEdtPhoto() {
        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_choose_image)
        val window: Window? = dialog.window
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val btnCamera = dialog.findViewById(R.id.btnCamera) as MaterialButton
        val btnGallery = dialog.findViewById(R.id.btnGallery) as MaterialButton
        val btnCancel = dialog.findViewById(R.id.btnCancel) as MaterialButton
        btnCamera.setOnClickListener {
            askForPermissions(true)
            dialog.dismiss()
        }

        btnGallery.setOnClickListener {
            askForPermissions(false)
            dialog.dismiss()
        }
        btnCancel.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { intent ->
            intent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createCapturedPhoto()
                } catch (ex: IOException) {
                    // If there is error while creating the File, it will be null
                    null
                }

                photoFile?.also {
                    val photoURI = FileProvider.getUriForFile(
                        Objects.requireNonNull(this),
                        BuildConfig.APPLICATION_ID + ".provider", it
                    )

                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createCapturedPhoto(): File {
        val timestamp: String = SimpleDateFormat("yyyyMMdd-HHmmss", Locale.US).format(Date())
        //val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val storageDir = cacheDir

        return File.createTempFile("PHOTO_${timestamp}", ".jpg", storageDir).apply {
            potoImage = this
            currentPhotoPath = absolutePath
        }
    }

    private fun openGallery() {
        Intent(Intent.ACTION_PICK).also {
            it.type = "image/*"
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            it.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(it, REQUEST_PICK_IMAGE)
        }
    }

    private fun uploadPicture() {
        if (uriImage == null) {
            toast("Select an Image First")
            return
        }
        try {
            val parcelFileDescriptor =
                contentResolver.openFileDescriptor(uriImage!!, "r", null) ?: return

            Log.d("parcelFileDescriptor", parcelFileDescriptor.toString())

            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            if (isCamera) {
//                val file = File(currentPhotoPath, contentResolver.getFileName(uriImage!!))
//                val outputStream = FileOutputStream(file)
//                inputStream.copyTo(outputStream)
                makeLoading(pDialog, true)
                uploadLoading(pDialog, "0 %")
                val body = UploadRequestBody(File(currentPhotoPath), "image", this)
                viewModel.postUploadMedia(
                    MultipartBody.Part.createFormData(
                        "file",
                        File(currentPhotoPath).name,
                        body
                    ), "avatar".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                )
            } else {
                val file = File(cacheDir, contentResolver.getFileName(uriImage!!))
                val outputStream = FileOutputStream(file)
                inputStream.copyTo(outputStream)
                makeLoading(pDialog, true)
                uploadLoading(pDialog, "0 %")
                val body = UploadRequestBody(file, "image", this)
                viewModel.postUploadMedia(
                    MultipartBody.Part.createFormData(
                        "file",
                        file.name,
                        body
                    ), "avatar".toRequestBody("multipart/form-data".toMediaTypeOrNull())
                )
            }

        } catch (exc: Exception) {
            Log.d("akundetail", exc.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                val uri = Uri.fromFile(File(currentPhotoPath))
                binding.imgEvent.setImageURI(uri)
                uriImage = uri
                isCamera = true
            } else if (requestCode == REQUEST_PICK_IMAGE) {
                val uri = data?.data
                binding.imgEvent.setImageURI(uri)
                uriImage = uri
                isCamera = false
            }
        }
    }

    private fun askForPermissions(isCamera: Boolean) {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                openCamera()
                            } else {
                                openGallery()
                            }
                        }

                        if (report.isAnyPermissionPermanentlyDenied) {
                            makeWarning(title = "Camera- en opslagvergunning vereist ",
                                content = "Gebruik van toestaan via instellingen?",
                                confirmListener = {
                                    val intent = Intent()
                                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                    val uri = Uri.fromParts(
                                        "package",
                                        packageName,
                                        null
                                    )
                                    intent.data = uri
                                    startActivity(intent)
                                    dialog.dismiss()
                                },
                                cancelListener = {
                                    dialog.dismiss()
                                })
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    // Remember to invoke this method when the custom rationale is closed
                    // or just by default if you don't want to use any custom rationale.
                    token?.continuePermissionRequest()
                }

            })
            .withErrorListener {
                Log.d("Account Detail", it.name)
            }
            .check()
    }

    override fun onProgressUpdate(percentage: Int) {
        uploadLoading(pDialog, "$percentage %")
    }
}