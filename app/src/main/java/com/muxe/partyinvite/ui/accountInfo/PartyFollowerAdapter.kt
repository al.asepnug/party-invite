package com.muxe.partyinvite.ui.accountInfo

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.databinding.ItemFollowerBinding
import com.muxe.partyinvite.utils.showImageRounded

class PartyFollowerAdapter(private val context: Context, private val partyListItem: ArrayList<Party>,
                           private val clickListener: (Party) -> Unit)
    : RecyclerView.Adapter<PartyFollowerAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemFollowerBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return partyListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(partyListItem[position])
    }

    inner class ViewHolder(private val binding: ItemFollowerBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Party) {
            data.cover?.url?.let { context.showImageRounded(it, binding.imageView) }

            itemView.setOnClickListener{
                clickListener(data)
            }
        }
    }

    fun updateData(newList: List<Party>?) {
        partyListItem.clear()
        newList?.let { partyListItem.addAll(it) }
        notifyDataSetChanged()
    }
}