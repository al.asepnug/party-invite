package com.muxe.partyinvite.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.User
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    fun getUser(): LiveData<User> = dataRepository.getUser()

    fun deleteAllLocalData() = dataRepository.deleteAllLocalData()

    fun removeAllSharedPref() = dataRepository.removeDataSharePref()
}