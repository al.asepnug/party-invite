package com.muxe.partyinvite.ui.followersDetail

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RatingBar
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.muxe.partyinvite.R.*
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.data.entity.Party
import com.muxe.partyinvite.data.entity.UserDetails
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.databinding.ActivityFollowersDetailBinding
import com.muxe.partyinvite.ui.allEvent.EventAdapter
import com.muxe.partyinvite.ui.pageEvent.PageEvent
import com.muxe.partyinvite.ui.reviewsList.ReviewsActivity
import com.muxe.partyinvite.utils.*
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException

@AndroidEntryPoint
class FollowersDetailActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivityFollowersDetailBinding
    private var following: Owner? = null
    private var followingDetails: UserDetails? = null
    private val viewModel: FollowersDetailViewModel by viewModels()
    private lateinit var pDialog: SweetAlertDialog
    private val listParty = ArrayList<Party>()

    companion object {
        const val DATA = "data_follower"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFollowersDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        pDialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
        binding.btnBack.setOnClickListener(this)
        binding.btnFollow.setOnClickListener(this)
        binding.btnShare.setOnClickListener(this)
        binding.viewSetReview.setOnClickListener(this)
        binding.viewGetReview.setOnClickListener(this)
        try {
            following = intent.extras?.getParcelable(DATA)
            initUi()
            updateUiParticipants()
            populateData()
            if (following?.id != null) {
                viewModel.getFullProfile(following?.id)
            }
        } catch (e: IOException) {
            toast("Error Load Data")
        }
    }

    private fun initUi() {
        binding.rvEvent.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = EventAdapter(viewModel.getLocation(), context, listParty, clickListener = {
                val intent = Intent(context, PageEvent::class.java)
                intent.putExtra(PageEvent.DATA, it)
                context.startActivity(intent)
            }, clickFav = {
                viewModel.getPartyFavorite(it)
            })
        }
    }

    @SuppressLint("SetTextI18n")
    private fun populateData() {
        viewModel.partyFavoriteResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        makeSuccess(title = "Success", content = "Successfully " +
                                "${it.message?.replace('_', ' ')}", confirmListener = {
                            viewModel.getFullProfile(following?.id)
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }

            }
        })

        viewModel.userDetailsResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    result.body?.let {
                        followingDetails = it
                        updateUiParticipantsDetails()
                        binding.rvEvent.adapter?.let { adapter ->
                            when (adapter) {
                                is EventAdapter -> {
                                    try {
                                        if (it.joined?.size!! > 0) {
                                            it.joined.let { it1 -> adapter.updateData(it1) }
                                        } else {
                                            toast("Data is Empty")
                                        }
                                    } catch (e: IOException) {
                                        toast("Error Load Data")
                                    }
                                }
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                }
            }
        })

        viewModel.messageResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        makeSuccess(title = "Success", content = "Successfully " +
                                "${it.status}"
                        ) {
                            if (it.status.equals("Followed")) {
                                binding.btnFollow.backgroundTintList = ContextCompat.getColorStateList(
                                    this, color.green)
                                binding.btnFollow.text = "Following"
                            } else if (it.status.equals("Unfollowed")) {
                                binding.btnFollow.backgroundTintList = ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        this,
                                        color.blue
                                    )
                                )
                                binding.btnFollow.text = "Follow"
                            }
                        }
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })

        viewModel.reviewsResponse.observe(this, { result ->
            when (result.status) {
                ApiResponse.StatusResponse.SUCCESS -> {
                    makeLoading(pDialog, false)
                    result.body?.let {
                        makeSuccess(title = "Success", content = "Successfully " +
                                it.message?.replace('_', ' '), confirmListener = {
                            viewModel.getFullProfile(following?.id)
                        })
                    }
                }

                ApiResponse.StatusResponse.ERROR -> {
                    makeLoading(pDialog, false)
                    result.message?.let { toast(it) }
                }

                ApiResponse.StatusResponse.LOADING -> {
                    makeLoading(pDialog, true)
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            id.btnBack -> {
                onBackPressed()
            }
            id.btnFollow -> {
                if (following?.isFollowing == true) {
                    viewModel.deleteFollow(following?.id)
                } else {
                    viewModel.postFollow(following?.id)
                }
            }
            id.btnShare -> {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(
                        Intent.EXTRA_TEXT,
                        "Volgen ${following?.firstname} ${following?.lastname}"
                    )
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, "Share Following")
                startActivity(shareIntent)
            }
            id.viewSetReview -> {
                showDialogReviews()
            }

            id.viewGetReview -> {
                val intent = Intent(this, ReviewsActivity::class.java)
                intent.putExtra(ReviewsActivity.DATA_RATING, followingDetails)
                startActivity(intent)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun showDialogReviews() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(layout.dialog_add_reviews)
        val window: Window? = dialog.window
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val title = dialog.findViewById(id.tvTitle) as TextView
        val yesBtn = dialog.findViewById(id.btnSave) as MaterialButton
        val noBtn = dialog.findViewById(id.btnCancel) as MaterialButton
        val edtMessage = dialog.findViewById(id.edtDescriptionVal) as TextInputEditText
        val ratingBar = dialog.findViewById(id.setRating) as RatingBar
        title.text = "Review schrijven"
        yesBtn.setOnClickListener {
            val field: HashMap<String?, Any?> = HashMap()
            field["user_id"] = following?.id
            field["review"] = edtMessage.text.toString()
            field["rating"] = ratingBar.rating
            viewModel.postReviews(field)
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun updateUiParticipants() {
        binding.tvNameDj.text = "${following?.firstname} ${following?.lastname}"
        following?.avatar?.url?.let { showImageRounded(it, binding.imgDj) }
        following?.cover?.url?.let { showImageCenterInside(it, binding.imgContent) }
        if (following?.isFollowing == true) {
            binding.btnFollow.text = "Following"
            binding.btnFollow.backgroundTintList = ContextCompat.getColorStateList(
                this, color.green)
        } else {
            binding.btnFollow.text = "Follow"
            binding.btnFollow.backgroundTintList = ContextCompat.getColorStateList(
                this, color.blue)
        }
        binding.setRating.rating = following?.rating?.toFloat() ?: 0F
        if (following?.ratingCount != null) {
            binding.tvGetRating.text = "${following?.ratingCount} recenties | Bekijk alle reviews"
            binding.getRating.rating = following?.ratingCount?.toFloat() ?: 0F
        }
    }

    private fun updateUiParticipantsDetails() {
        binding.tvFollowerVal.text = followingDetails?.followers?.size.toString()
        binding.tvProfileVal.text = followingDetails?.bio
    }
}