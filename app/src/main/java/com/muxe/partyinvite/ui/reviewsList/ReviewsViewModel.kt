package com.muxe.partyinvite.ui.reviewsList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Reviews
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewsViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    private val _reviewsResponse = MutableLiveData<ApiResponse<List<Reviews>>>()

    val reviewsResponse = _reviewsResponse

    fun getReviews(idUser: Int?) {
        viewModelScope.launch {
            dataRepository.getReviews(idUser).collect{
                _reviewsResponse.value = it
            }
        }
    }
}