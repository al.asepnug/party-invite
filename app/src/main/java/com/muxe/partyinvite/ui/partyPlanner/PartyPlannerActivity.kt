package com.muxe.partyinvite.ui.partyPlanner

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.muxe.partyinvite.R

class PartyPlannerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_party_planner)
    }
}