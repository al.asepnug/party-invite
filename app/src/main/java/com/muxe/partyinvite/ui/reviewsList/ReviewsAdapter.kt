package com.muxe.partyinvite.ui.reviewsList

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.data.entity.Reviews
import com.muxe.partyinvite.databinding.ItemReviewsBinding

class ReviewsAdapter (private val reviewsListItem: ArrayList<Reviews>)
    : RecyclerView.Adapter<ReviewsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemReviewsBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return reviewsListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(reviewsListItem[position])
    }

    inner class ViewHolder(private val binding: ItemReviewsBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: Reviews) {
           binding.tvDescription.text = data.review
            if (data.rating != null) {
                binding.setRating.rating = data.rating.toFloat()
            }
        }
    }

    fun updateData(newList: List<Reviews>) {
        reviewsListItem.clear()
        reviewsListItem.addAll(newList)
        notifyDataSetChanged()
    }
}