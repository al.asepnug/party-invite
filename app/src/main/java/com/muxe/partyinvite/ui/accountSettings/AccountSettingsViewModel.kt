package com.muxe.partyinvite.ui.accountSettings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.muxe.partyinvite.data.DataRepository
import com.muxe.partyinvite.data.entity.Message
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.data.remote.ApiResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AccountSettingsViewModel @Inject constructor(
    private val dataRepository: DataRepository
) : ViewModel() {
    fun getUser(): LiveData<User> = dataRepository.getUser()

    fun deleteAllLocalData() = dataRepository.deleteAllLocalData()

    fun removeAllSharedPref() = dataRepository.removeDataSharePref()

}