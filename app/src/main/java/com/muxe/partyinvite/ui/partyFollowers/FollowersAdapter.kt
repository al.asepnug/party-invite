package com.muxe.partyinvite.ui.partyFollowers

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.muxe.partyinvite.R
import com.muxe.partyinvite.data.entity.Owner
import com.muxe.partyinvite.databinding.ItemDjBinding
import com.muxe.partyinvite.utils.showImageRounded

class FollowersAdapter (private val context: Context, private val followingListItem: ArrayList<Owner>,
                        private val clickListener: (Owner) -> Unit, private val clickFollowing: (Owner) -> Unit)
    : RecyclerView.Adapter<FollowersAdapter.ViewHolder>(){

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemDjBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(
            binding
        )
    }

    override fun getItemCount(): Int {
        return followingListItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(followingListItem[position])
    }

    inner class ViewHolder(private val binding: ItemDjBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(data: Owner) {
            binding.tvNameDj.text = "${data.firstname} ${data.lastname}"
            binding.tvProducer.text = "producer not provide api"
            binding.tvFollowerVal.text = "not provide api"
            if (data.isFollowing == true) {
                binding.btnFollowing.text = "Following"
                binding.btnFollowing.setTextColor(ContextCompat.getColor(context, R.color.green))
            } else {
                binding.btnFollowing.text = "Follow"
                binding.btnFollowing.setTextColor(ContextCompat.getColor(context, R.color.blue))
            }
            data.avatar?.url?.let { context.showImageRounded(it, binding.imgDj) }
            itemView.setOnClickListener{
                clickListener(data)
            }

            binding.btnFollowing.setOnClickListener{
                clickFollowing(data)
            }
        }
    }

    fun updateData(newList: List<Owner>) {
        followingListItem.clear()
        followingListItem.addAll(newList)
        notifyDataSetChanged()
    }
}