package com.muxe.partyinvite.di

import android.content.Context
import androidx.room.Room
import com.muxe.partyinvite.data.local.db.AppDatabase
import com.muxe.partyinvite.data.local.db.DatabaseDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "invite_party.db",
        ).build()
    }

    @Provides
    fun provideDao(appDatabase: AppDatabase): DatabaseDao {
        return appDatabase.databaseDao()
    }
}