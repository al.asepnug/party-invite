package com.muxe.partyinvite.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.muxe.partyinvite.data.entity.PaymentsItem

class ConvertersPayment {
    @TypeConverter
    fun fromString(value: String): List<PaymentsItem?>? {
        val listType = object : TypeToken<List<PaymentsItem>>() {
        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: List<PaymentsItem>): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}