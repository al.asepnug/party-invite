package com.muxe.partyinvite.utils

import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.provider.OpenableColumns
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import cn.pedant.SweetAlert.SweetAlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.snackbar.Snackbar
import com.muxe.partyinvite.R
import java.io.File


fun Context.toast(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun Activity.snackBar(message: CharSequence) =
    Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT).show()

fun Activity.snackBarWithAction(
    message: CharSequence,
    actionMsg: CharSequence,
    actionListener: () -> Unit
) {
    val snackBar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
    snackBar.setAction(actionMsg) {
        actionListener()
    }
    snackBar.show()
}

fun Activity.makeStatusBarTransparent() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            }
            statusBarColor = Color.TRANSPARENT
        }
    } else {
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }
}

fun Context.makeLoading(pDialog: SweetAlertDialog, isShow: Boolean) {
    if (isShow) {
        pDialog.progressHelper.barColor = ContextCompat.getColor(this, R.color.orange_700)
        pDialog.titleText = getString(R.string.LOADING)
        pDialog.setCancelable(false)
        pDialog.show()
    } else {
        pDialog.dismiss()
    }
}

fun Context.uploadLoading(pDialog: SweetAlertDialog, title: String) {
    pDialog.titleText = title
}

fun Context.makeWarning(
    title: String,
    content: String,
    confirmListener: () -> Unit,
    cancelListener: () -> Unit
) {
    val pDialog = SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        .setTitleText(title)
        .setContentText(content)
        .setConfirmText("Yes")
        .setConfirmClickListener { sDialog ->
            sDialog.dismissWithAnimation()
            confirmListener()
        }
        .setCancelButton("No") { sDialog ->
            sDialog.dismissWithAnimation()
            cancelListener()
        }

    pDialog.confirmButtonBackgroundColor = ContextCompat.getColor(this, R.color.orange_700)
    pDialog.cancelButtonBackgroundColor = ContextCompat.getColor(this, R.color.red)
    pDialog.setCanceledOnTouchOutside(false)
    pDialog.show()
}

fun Context.makeSuccess(title: String, content: String, confirmListener: () -> Unit) {
    val pDialog = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
        .setTitleText(title)
        .setContentText(content)
        .setConfirmText("OK")
        .setConfirmClickListener { sDialog ->
            sDialog.dismissWithAnimation()
            confirmListener()
        }
    pDialog.confirmButtonBackgroundColor = ContextCompat.getColor(this, R.color.orange_700)
    pDialog.setCanceledOnTouchOutside(false)
    pDialog.show()
}

fun Context.showImageCenterCrop(content: Any, view: ImageView) {
    Glide.with(this)
        .load(content)
        .centerCrop()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}

fun Context.showImageCenterInside(content: Any, view: ImageView) {
    Glide.with(this)
        .load(content)
        .centerInside()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}

fun Context.showImageCircleCrop(content: Any, view: ImageView) {
    Glide.with(this)
        .load(content)
        .circleCrop()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}

fun Context.showImageRounded(content: Any, view: ImageView) {
    Glide.with(this)
        .load(content)
        .transform(CenterCrop(), RoundedCorners(18))
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(view)
}

fun File.calculateSizeRecursively(): Long {
    return walkBottomUp().fold(0L, { acc, file -> acc + file.length() })
}

fun Group.addOnClickListener(listener: (view: View) -> Unit) {
    referencedIds.forEach { id ->
        rootView.findViewById<View>(id).setOnClickListener(listener)
    }
}

fun ContentResolver.getFileName(fileUri: Uri): String {
    var name = ""
    val returnCursor = this.query(fileUri, null, null, null, null)
    if (returnCursor != null) {
        val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        name = returnCursor.getString(nameIndex)
        returnCursor.close()
    }
    return name
}
