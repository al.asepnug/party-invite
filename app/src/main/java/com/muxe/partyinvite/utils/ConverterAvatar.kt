package com.muxe.partyinvite.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.muxe.partyinvite.data.entity.Avatar

class ConverterAvatar {
    @TypeConverter
    fun fromString(value: String?): Avatar? {
        val listType = object : TypeToken<Avatar>() {
        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: Avatar?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}