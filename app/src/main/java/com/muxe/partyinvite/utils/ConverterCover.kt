package com.muxe.partyinvite.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.muxe.partyinvite.data.entity.Cover

class ConverterCover {
    @TypeConverter
    fun fromString(value: String?): Cover? {
        val listType = object : TypeToken<Cover>() {
        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: Cover?): String? {
        val gson = Gson()
        return gson.toJson(list)
    }
}