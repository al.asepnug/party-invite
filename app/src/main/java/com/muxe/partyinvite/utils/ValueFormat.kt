package com.muxe.partyinvite.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object ValueFormat {
    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun formatDateTimed(date: String?): String? {
        @SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val myDate: Date?
        var finalDate: String? = null
        try {
            myDate = rawFormat.parse(date!!)
            val dateFormat = SimpleDateFormat("dd MMMM, yyyy", Locale("nl", "NL"))
             val timeFormat = SimpleDateFormat("HH:mm", Locale("nl", "NL"))
            if (myDate != null) {
                finalDate = "${dateFormat.format(myDate)} | ${timeFormat.format(myDate)}"
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return finalDate
    }

    fun formatDateTimed2(date: String?): String? {
        @SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val myDate: Date?
        var finalDate: String? = null
        try {
            myDate = rawFormat.parse(date!!)
            val dateFormat = SimpleDateFormat("dd MMMM, yyyy", Locale("nl", "NL"))
            val timeFormat = SimpleDateFormat("HH:mm", Locale("nl", "NL"))
            if (myDate != null) {
                finalDate = "${dateFormat.format(myDate)} at ${timeFormat.format(myDate)}"
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return finalDate
    }

    fun formatDateTime(date: String?): String? {
        ///@SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        @SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd")
        val myDate: Date?
        var finalDate: String? = null
        try {
            myDate = rawFormat.parse(date!!)
            val dateFormat = SimpleDateFormat("dd MMMM, yyyy", Locale("nl", "NL"))
           // val timeFormat = SimpleDateFormat("HH:mm", Locale("nl", "NL"))
            if (myDate != null) {
//                finalDate = "${dateFormat.format(myDate)} |  ${timeFormat.format(myDate)}"
                finalDate = "${dateFormat.format(myDate)}"
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return finalDate
    }

    fun formatDate(date: String?): String? {
//        @SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        @SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val myDate: Date?
        var finalDate: String? = null
        try {
            myDate = rawFormat.parse(date!!)
            val dateFormat = SimpleDateFormat("dd MMM EEEE", Locale("nl", "NL"))
            if (myDate != null) {
                finalDate = dateFormat.format(myDate)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return finalDate
    }

    fun formatTime(date: String?): String? {
        @SuppressLint("SimpleDateFormat") val rawFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        val myDate: Date?
        var finalDate: String? = null
        try {
            myDate = rawFormat.parse(date!!)
            val timeFormat = SimpleDateFormat("HH:mm", Locale("nl", "NL"))
            if (myDate != null) {
                finalDate = timeFormat.format(myDate)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return finalDate
    }

    @SuppressLint("SimpleDateFormat")
    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        //2021-08-30 20:20:40
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(date)
    }

    fun formatDateNow(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        return sdf.format(Date()).toString()
    }

    fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(
                intrinsicWidth,
                intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    fun formatRound(data : Double?): String {
        return String.format("%.2f", data)
    }
}