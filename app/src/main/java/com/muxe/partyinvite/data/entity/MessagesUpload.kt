package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MessagesUpload(

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("key")
	val key: String? = null,

	@field:SerializedName("url")
	val url: String? = null
) : Parcelable
