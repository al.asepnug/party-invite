package com.muxe.partyinvite.data

import androidx.lifecycle.LiveData
import com.muxe.partyinvite.data.entity.*
import com.muxe.partyinvite.data.local.db.LocalDataSource
import com.muxe.partyinvite.data.local.prefs.PrefsDataSource
import com.muxe.partyinvite.data.remote.ApiResponse
import com.muxe.partyinvite.data.remote.RemoteDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class DataRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val prefsDataSource: PrefsDataSource,
    private val localDataSource: LocalDataSource
) : DataSource {

    override fun getIsLogin(): Boolean = prefsDataSource.getIsLogin()

    override fun setIsLogin(isLogin: Boolean) {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.setIsLogin(isLogin)
        }
    }

    override fun getLocation(): Location = prefsDataSource.getLocation()

    override fun setLocation(value: Location) {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.setLocation(value)
        }
    }

    override fun removeDataSharePref() {
        CoroutineScope(Dispatchers.IO).launch {
            prefsDataSource.removeAllData()
        }
    }

    override suspend fun postLogin(body: String?): Flow<ApiResponse<Login?>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postLogin(body)
            if (!result.body?.token.isNullOrEmpty()) {
                result.body?.token?.let {
                    prefsDataSource.setIsLogin(true)
                    prefsDataSource.setToken("Bearer $it")
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postRegister(fields: HashMap<String?, RequestBody?>): Flow<ApiResponse<Register?>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postRegister(fields)
            if (result.body?.status.equals("social_login_ok") || result.body?.status.equals("registration_complete")) {
                result.body?.token.let {
                    prefsDataSource.setIsLogin(true)
                    prefsDataSource.setToken("Bearer $it")
                }
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getAuth(): Flow<ApiResponse<User?>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getAuth(prefsDataSource.getToken())
            if (result.body != null) {
                localDataSource.insertUser(result.body)
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postParty(fields: HashMap<String?, RequestBody?>): Flow<ApiResponse<PartyCreated>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postParty(prefsDataSource.getToken(), fields)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getParty(path: String?): Flow<ApiResponse<List<Party>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getParty(prefsDataSource.getToken(), path)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getPartySearch(q: String, period: String): Flow<ApiResponse<List<Party>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getPartySearch(prefsDataSource.getToken(), q, period)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getPartyType(): Flow<ApiResponse<List<Type>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getPartyType(prefsDataSource.getToken())
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getMusicType(): Flow<ApiResponse<List<Type>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getMusicType(prefsDataSource.getToken())
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getPartyNearby(latitude: String?, longitude: String?): Flow<ApiResponse<List<Party>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getPartyNearby(prefsDataSource.getToken(), latitude, longitude)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getPartyById(id: Int?): Flow<ApiResponse<Party>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getPartyById(prefsDataSource.getToken(), id)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getPartyFavorite(id: Int?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getPartyFavorite(prefsDataSource.getToken(), id)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getJoinParty(id: Int?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getJoinParty(prefsDataSource.getToken(), id)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getMessages(typeMessages: String?, id: Int?): Flow<ApiResponse<List<Messages>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getMessagesParty(prefsDataSource.getToken(), typeMessages, id)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postLikeMessages(like: String?, id: Int?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postLikeMessagesParty(prefsDataSource.getToken(), like, id)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postMessages(typeMessages: String?, id: Int?, message: String?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postMessagesParty(prefsDataSource.getToken(), typeMessages, id, message)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getTickets(): Flow<ApiResponse<List<Tickets>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getTickets(prefsDataSource.getToken())
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getTicketById(id: Int?): Flow<ApiResponse<Tickets>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getTicketById(prefsDataSource.getToken(), id)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getProfile(): Flow<ApiResponse<User>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getProfile(prefsDataSource.getToken())
            if (result.body != null) {
                localDataSource.insertUser(result.body)
            }
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getFullProfile(idUser: Int?): Flow<ApiResponse<UserDetails>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getFullProfile(prefsDataSource.getToken(), idUser)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun putProfile(body: String?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.putProfile(prefsDataSource.getToken(), body)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getFollowing(): Flow<ApiResponse<List<Owner>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getFollowing(prefsDataSource.getToken())
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postFollow(idUser: Int?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postFollow(prefsDataSource.getToken(), idUser)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun deleteFollow(idUser: Int?): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.deleteFollow(prefsDataSource.getToken(), idUser)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getReviews(idUser: Int?): Flow<ApiResponse<List<Reviews>>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getReviews(prefsDataSource.getToken(), idUser)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postReviews(fields: HashMap<String?, Any?>): Flow<ApiResponse<Message>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postReviews(prefsDataSource.getToken(), fields)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getFacebookData(
        userId: String,
        authHeader: String): Flow<ApiResponse<FacebookResponse?>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.getFacebookData( userId, authHeader)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun postUploadMedia(
        image: MultipartBody.Part,
        key: RequestBody
    ): Flow<ApiResponse<MessagesUpload>> {
        return flow {
            emit(ApiResponse.loading())
            val result = remoteDataSource.postUploadMedia(prefsDataSource.getToken(), image, key)
            emit(result)
        }.flowOn(Dispatchers.IO)
    }

    override fun getUser(): LiveData<User> = localDataSource.getUser()

    override fun insertUser(user: User) {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.insertUser(user)
        }
    }

    override fun deleteAllLocalData() {
        CoroutineScope(Dispatchers.IO).launch {
            localDataSource.deleteAllUser()
        }
    }
}