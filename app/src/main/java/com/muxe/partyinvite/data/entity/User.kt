package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "user")
@Parcelize
data class User(
	@PrimaryKey
	@NonNull
	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("avatar")
	@Nullable
	val avatar: Avatar? = null,

	@field:SerializedName("firstname")
	val firstname: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("lastname")
	val lastname: String? = null,

	@field:SerializedName("rating_raw")
	val ratingRaw: Double? = null,

	@field:SerializedName("cover")
	val cover: Cover? = null,

	@field:SerializedName("rating")
	val rating: Double? = null,

	@field:SerializedName("bio")
	val bio: String? = null,

	@field:SerializedName("rating_count")
	val ratingCount: Int? = null
) : Parcelable
