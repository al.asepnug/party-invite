package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDetails(

	@field:SerializedName("rating_raw")
	val ratingRaw: Double? = null,

	@field:SerializedName("favorites")
	val favorites: List<Party?>? = null,

	@field:SerializedName("firstname")
	val firstname: String? = null,

	@field:SerializedName("tickets")
	val tickets: List<Tickets?>? = null,

	@field:SerializedName("joined")
	val joined: List<Party>? = null,

	@field:SerializedName("payments")
	val payments: List<PaymentsItem>? = null,

	@field:SerializedName("rating")
	val rating: Double? = null,

	@field:SerializedName("bio")
	val bio: String? = null,

	@field:SerializedName("avatar")
	val avatar: Avatar? = null,

	@field:SerializedName("lastname")
	val lastname: String? = null,

	@field:SerializedName("rating_count")
	val ratingCount: Int? = null,

	@field:SerializedName("cover")
	val cover: Cover? = null,

	@field:SerializedName("followers")
	val followers: List<Owner>? = null,

	@field:SerializedName("owned")
	val owned: List<Party?>? = null,

	@field:SerializedName("following")
	val following: List<Owner>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
