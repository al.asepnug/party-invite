package com.muxe.partyinvite.data.local.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.muxe.partyinvite.data.entity.User

@Dao
interface DatabaseDao {
    //user
    @Query("SELECT * FROM user LIMIT 1")
    fun getUser() : LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = User::class)
    suspend fun insertUser(user: User)

    @Delete
    fun deleteUser(user: User)

    @Query("DELETE FROM user")
    suspend fun deleteAllUser()
}