package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Messages(

	@field:SerializedName("wall_id")
	val wallId: Int? = null,

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("owner")
	val owner: Owner? = null,

	@field:SerializedName("dislikes")
	val dislikes: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("like_state")
	val likeState: Int? = null,

	@field:SerializedName("likes")
	val likes: String? = null
) : Parcelable
