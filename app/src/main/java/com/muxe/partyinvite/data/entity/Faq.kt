package com.muxe.partyinvite.data.entity

import com.google.gson.annotations.SerializedName

data class Faq(
    @field:SerializedName("title")
    val title: String,

    @field:SerializedName("description")
    val description: String,

    var expand : Boolean = false
)