package com.muxe.partyinvite.data.local.prefs

import android.content.Context
import com.muxe.partyinvite.data.entity.Location
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrefsDataSource @Inject constructor(@ApplicationContext context: Context) {
    companion object {
        const val PREFERENCE_NAME = "PARTY_INVITE_PREF"
        const val IS_LOGIN = "is_login"
        const val TOKEN = "token"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
    }

    private val preferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun removeAllData() {
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }

    fun setIsLogin(value: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(IS_LOGIN, value)
        editor.apply()
    }

    fun getIsLogin(): Boolean {
        return preferences.getBoolean(IS_LOGIN, false)
    }

    fun setToken(value: String) {
        val editor = preferences.edit()
        editor.putString(TOKEN, value)
        editor.apply()
    }

    fun getToken(): String? {
        return preferences.getString(TOKEN, "")
    }

    fun setLocation(value: Location) {
        val editor = preferences.edit()
        editor.putString(LATITUDE, value.latitude)
        editor.putString(LONGITUDE, value.longitude)
        editor.apply()
    }

    fun getLocation() : Location {
        return Location(preferences.getString(LATITUDE, "0"), preferences.getString(LONGITUDE, "0"))
    }
}