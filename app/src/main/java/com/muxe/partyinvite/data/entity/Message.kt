package com.muxe.partyinvite.data.entity

import com.google.gson.annotations.SerializedName

data class Message(

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("available")
    val available: Int? = null,

    @field:SerializedName("error")
    val error: String? = null
)
