package com.muxe.partyinvite.data.entity

import com.google.gson.annotations.SerializedName

data class Register(

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("token")
	val token: String? = null
)
