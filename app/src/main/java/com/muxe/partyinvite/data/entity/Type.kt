package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Type(

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("label")
	val label: String
) : Parcelable {
	override fun toString(): String {
		return label
	}
}
