package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Owner(

    @field:SerializedName("cover")
    val cover: Cover? = null,

    @field:SerializedName("avatar")
    val avatar: Avatar? = null,

    @field:SerializedName("firstname")
    val firstname: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("lastname")
    val lastname: String? = null,

    @field:SerializedName("bio")
    val bio: String? = null,

    @field:SerializedName("rating")
    val rating: Double? = null,

    @field:SerializedName("rating_raw")
    val ratingRaw: Double? = null,

    @field:SerializedName("rating_count")
    val ratingCount: Int? = null,

    @field:SerializedName("is_following")
    val isFollowing: Boolean? = null
) : Parcelable