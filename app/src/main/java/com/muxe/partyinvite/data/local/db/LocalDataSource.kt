package com.muxe.partyinvite.data.local.db

import com.muxe.partyinvite.data.entity.User
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val databaseDao: DatabaseDao) {
    // user
    suspend fun insertUser(user: User) = databaseDao.insertUser(user)

    fun getUser() = databaseDao.getUser()

    suspend fun deleteAllUser() = databaseDao.deleteAllUser()
}