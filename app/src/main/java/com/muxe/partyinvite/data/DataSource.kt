package com.muxe.partyinvite.data

import androidx.lifecycle.LiveData
import com.muxe.partyinvite.data.entity.*
import com.muxe.partyinvite.data.remote.ApiResponse
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface DataSource {
    fun getIsLogin(): Boolean

    fun setIsLogin(isLogin: Boolean)

    fun getLocation(): Location

    fun setLocation(value: Location)

    fun removeDataSharePref()

    suspend fun postLogin(body: String?) : Flow<ApiResponse<Login?>>

    suspend fun postRegister(fields: HashMap<String?, RequestBody?>) : Flow<ApiResponse<Register?>>

    suspend fun getAuth() : Flow<ApiResponse<User?>>

    suspend fun postParty(fields: HashMap<String?, RequestBody?>) : Flow<ApiResponse<PartyCreated>>

    suspend fun getParty(path: String?) : Flow<ApiResponse<List<Party>>>

    suspend fun getPartySearch(q: String, period: String) : Flow<ApiResponse<List<Party>>>

    suspend fun getPartyType() : Flow<ApiResponse<List<Type>>>

    suspend fun getMusicType() : Flow<ApiResponse<List<Type>>>

    suspend fun getPartyNearby(latitude: String?, longitude: String?) : Flow<ApiResponse<List<Party>>>

    suspend fun getPartyById(id: Int?) : Flow<ApiResponse<Party>>

    suspend fun getPartyFavorite(id: Int?) : Flow<ApiResponse<Message>>

    suspend fun getJoinParty(id: Int?) : Flow<ApiResponse<Message>>

    suspend fun getMessages(typeMessages: String?, id: Int?) : Flow<ApiResponse<List<Messages>>>

    suspend fun postLikeMessages(like: String?, id: Int?) : Flow<ApiResponse<Message>>

    suspend fun postMessages(typeMessages: String?, id: Int?, message: String?) : Flow<ApiResponse<Message>>

    suspend fun getTickets() : Flow<ApiResponse<List<Tickets>>>

    suspend fun getTicketById(id: Int?) : Flow<ApiResponse<Tickets>>

    suspend fun getProfile() : Flow<ApiResponse<User>>

    suspend fun getFullProfile(idUser: Int?) : Flow<ApiResponse<UserDetails>>

    suspend fun putProfile(body: String?) : Flow<ApiResponse<Message>>

    suspend fun getFollowing() : Flow<ApiResponse<List<Owner>>>

    suspend fun postFollow(idUser: Int?) : Flow<ApiResponse<Message>>

    suspend fun deleteFollow(idUser: Int?) : Flow<ApiResponse<Message>>

    suspend fun getReviews(idUser: Int?) : Flow<ApiResponse<List<Reviews>>>

    suspend fun postReviews(fields: HashMap<String?, Any?>) : Flow<ApiResponse<Message>>

    suspend fun getFacebookData(userId: String, authHeader: String) : Flow<ApiResponse<FacebookResponse?>>

    suspend fun postUploadMedia(image: MultipartBody.Part, key: RequestBody) : Flow<ApiResponse<MessagesUpload>>

    fun getUser() : LiveData<User>

    fun insertUser(user: User)

    fun deleteAllLocalData()
}