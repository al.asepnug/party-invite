package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PartyCreated(

	@field:SerializedName("mutations")
	val mutations: List<String?>? = null,

	@field:SerializedName("partyData")
	val partyData: Party? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("message")
	val message: String? = null
) : Parcelable

