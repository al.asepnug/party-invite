package com.muxe.partyinvite.data.remote

import com.muxe.partyinvite.data.entity.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @Headers("Content-type:application/json")
    @POST("login_check")
    suspend fun postLogin(@Body body: String?): Response<Login?>

    @Multipart
    @POST("register")
    suspend fun postRegister(@PartMap fields: HashMap<String?, RequestBody?>): Response<Register?>

    @GET("auth")
    suspend fun getAuth(@Header("Authorization") authHeader: String?): Response<User?>

    @Multipart
    @POST("party")
    suspend fun postParty(
        @Header("Authorization") authHeader: String?,
        @PartMap fields: HashMap<String?, RequestBody?>
    ): Response<PartyCreated>

    @GET("party/{path}")
    suspend fun getParty(@Header("Authorization") authHeader: String?,
                         @Path("path") path: String?): Response<List<Party>>

    @GET("partytype")
    suspend fun getPartyType(@Header("Authorization") authHeader: String?): Response<List<Type>>

    @GET("party/search/{q}/{period}")
    suspend fun getPartySearch(
        @Header("Authorization") authHeader: String?,
        @Path("q") q: String,
        @Path("period") period: String
    ): Response<List<Party>>

    @GET("musictype")
    suspend fun getMusicType(@Header("Authorization") authHeader: String?): Response<List<Type>>

    @GET("party/nearby/{latitude}/{longitude}")
    suspend fun getPartyNearby(
        @Header("Authorization") authHeader: String?, @Path("latitude") latitude: String?,
        @Path("longitude") longitude: String?
    ): Response<List<Party>>

    @GET("party/get/{id}")
    suspend fun getPartyById(
        @Header("Authorization") authHeader: String?,
        @Path("id") id: Int?
    ): Response<Party>

    @GET("party/favorite/{id}")
    suspend fun getPartyFavorite(
        @Header("Authorization") authHeader: String?,
        @Path("id") id: Int?
    ): Response<Message>

    @GET("party/join/{id}")
    suspend fun getJoinParty(
        @Header("Authorization") authHeader: String?,
        @Path("id") id: Int?
    ): Response<Message>

    @GET("party/messages/{typeMessage}/{id}")
    suspend fun getMessagesParty(
        @Header("Authorization") authHeader: String?,
        @Path("typeMessage") typeMessage: String?,
        @Path("id") id: Int?
    ): Response<List<Messages>>

    @POST("party/messages/{like}/{id}")
    suspend fun postLikeMessagesParty(
        @Header("Authorization") authHeader: String?,
        @Path("like") like: String?,
        @Path("id") id: Int?
    ): Response<Message>

    @Multipart
    @POST("party/messages/{typeMessage}/{id}")
    suspend fun postMessagesParty(
        @Header("Authorization") authHeader: String?, @Path("typeMessage") typeMessage: String?,
        @Path("id") id: Int?,
        @Part("message") message: String?
    ): Response<Message>

    @GET("tickets")
    suspend fun getTickets(@Header("Authorization") authHeader: String?): Response<List<Tickets>>

    @GET("ticket/{id}")
    suspend fun getTicketById(
        @Header("Authorization") authHeader: String?,
        @Path("id") id: Int?
    ): Response<Tickets>

    @GET("profile")
    suspend fun getProfile(@Header("Authorization") authHeader: String?): Response<User>

    @GET("fullprofile/{idUser}")
    suspend fun getFullProfile(@Header("Authorization") authHeader: String?,
                           @Path("idUser") idUser: Int?): Response<UserDetails>

    @Headers("Content-type:application/json")
    @PUT("profile/update")
    suspend fun putProfile(
        @Header("Authorization") authHeader: String?,
        @Body body: String?
    ): Response<Message>

    @GET("following")
    suspend fun getFollowing(@Header("Authorization") authHeader: String?): Response<List<Owner>>

    @Multipart
    @POST("follow")
    suspend fun postFollow(
        @Header("Authorization") authHeader: String?,
        @Part("user") idUser: Int?
    ): Response<Message>

    @DELETE("unfollow/{idUser}")
    suspend fun deleteFollow(
        @Header("Authorization") authHeader: String?,
        @Path("idUser") idUser: Int?
    ): Response<Message>

    @GET("reviews/{idUser}")
    suspend fun getReviews(@Header("Authorization") authHeader: String?,
                             @Path("idUser") idUser: Int?): Response<List<Reviews>>

    @Multipart
    @POST("review")
    suspend fun postReview(
        @Header("Authorization") authHeader: String?,
        @PartMap fields: HashMap<String?, Any?>
    ): Response<Message>

    @GET
    suspend fun getFacebookData(
        @Url url: String,
        @Header("Authorization") authHeader: String,
        @Query("fields") fields: String,
        @Query("type") type: String
    ): Response<FacebookResponse?>

    @Multipart
    @POST("media/upload")
    suspend fun postUploadMedia(
        @Header("Authorization") authHeader: String?,
        @Part image: MultipartBody.Part,
        @Part("key") key: RequestBody
    ): Response<MessagesUpload>
}