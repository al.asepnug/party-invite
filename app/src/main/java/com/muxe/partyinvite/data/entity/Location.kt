package com.muxe.partyinvite.data.entity

class Location (
    val latitude: String?,

    val longitude: String?,
)