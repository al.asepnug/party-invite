package com.muxe.partyinvite.data.remote

import android.util.Log
import com.google.gson.Gson
import com.muxe.partyinvite.data.entity.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import retrofit2.Retrofit
import java.net.SocketTimeoutException
import javax.inject.Inject


class RemoteDataSource @Inject constructor(private val retrofit: Retrofit) {
    suspend fun postLogin(body: String?): ApiResponse<Login?> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postLogin(body) },
            defaultErrorMessage = "Login Failure"
        )
    }

    suspend fun postRegister(fields: HashMap<String?, RequestBody?>): ApiResponse<Register?> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postRegister(fields) },
            defaultErrorMessage = "Register Failure"
        )
    }

    suspend fun getAuth(authHeader: String?): ApiResponse<User?> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getAuth(authHeader) },
            defaultErrorMessage = "Get Auth Failure"
        )
    }

    suspend fun postParty(authHeader: String?, fields: HashMap<String?, RequestBody?>): ApiResponse<PartyCreated> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postParty(authHeader, fields) },
            defaultErrorMessage = "Post Register Failure"
        )
    }

    suspend fun getParty(authHeader: String?, path: String?): ApiResponse<List<Party>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getParty(authHeader, path) },
            defaultErrorMessage = "Get Party Failure"
        )
    }

    suspend fun getPartySearch(authHeader: String?, q: String, period: String): ApiResponse<List<Party>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getPartySearch(authHeader, q, period) },
            defaultErrorMessage = "Get Search Party Failure"
        )
    }

    suspend fun getPartyType(authHeader: String?): ApiResponse<List<Type>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getPartyType(authHeader) },
            defaultErrorMessage = "Get Party Type Failure"
        )
    }

    suspend fun getMusicType(authHeader: String?): ApiResponse<List<Type>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getMusicType(authHeader) },
            defaultErrorMessage = "Get Music Type Failure"
        )
    }

    suspend fun getPartyNearby(authHeader: String?, latitude: String?, longitude: String?): ApiResponse<List<Party>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getPartyNearby(authHeader, latitude, longitude) },
            defaultErrorMessage = "Get Party Nearby Failure"
        )
    }

    suspend fun getPartyById(authHeader: String?, id: Int?): ApiResponse<Party> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getPartyById(authHeader, id) },
            defaultErrorMessage = "Get Party By Id Failure"
        )
    }

    suspend fun getPartyFavorite(authHeader: String?, id: Int?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getPartyFavorite(authHeader, id) },
            defaultErrorMessage = "Get Party Favorite"
        )
    }

    suspend fun getJoinParty(authHeader: String?, id: Int?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getJoinParty(authHeader, id) },
            defaultErrorMessage = "Get Join Party"
        )
    }

    suspend fun getMessagesParty(authHeader: String?, typeMessage: String?, id: Int?): ApiResponse<List<Messages>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getMessagesParty(authHeader, typeMessage, id) },
            defaultErrorMessage = "Get Messages Party"
        )
    }

    suspend fun postLikeMessagesParty(authHeader: String?, like: String?, id: Int?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postLikeMessagesParty(authHeader, like, id) },
            defaultErrorMessage = "Post $like Messages Party"
        )
    }

    suspend fun postMessagesParty(authHeader: String?, typeMessage: String?, id: Int?, message: String?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postMessagesParty(authHeader, typeMessage, id, message) },
            defaultErrorMessage = "Post $typeMessage Messages Party"
        )
    }

    suspend fun getTickets(authHeader: String?): ApiResponse<List<Tickets>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getTickets(authHeader) },
            defaultErrorMessage = "Get Tickets Party"
        )
    }

    suspend fun getTicketById(authHeader: String?, id: Int?): ApiResponse<Tickets> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getTicketById(authHeader, id) },
            defaultErrorMessage = "Get Ticket By Id Party"
        )
    }

    suspend fun getProfile(authHeader: String?): ApiResponse<User> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getProfile(authHeader) },
            defaultErrorMessage = "Get Profile"
        )
    }

    suspend fun getFullProfile(authHeader: String?, idUser: Int?): ApiResponse<UserDetails> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getFullProfile(authHeader, idUser) },
            defaultErrorMessage = "Get Full Profile"
        )
    }

    suspend fun putProfile(authHeader: String?, body: String?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.putProfile(authHeader, body) },
            defaultErrorMessage = "Put Profile Failure"
        )
    }

    suspend fun getFollowing(authHeader: String?): ApiResponse<List<Owner>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getFollowing(authHeader) },
            defaultErrorMessage = "Get Following"
        )
    }

    suspend fun postFollow(authHeader: String?, idUser: Int?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postFollow(authHeader, idUser) },
            defaultErrorMessage = "Post Follow Failure"
        )
    }

    suspend fun deleteFollow(authHeader: String?, idUser: Int?): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.deleteFollow(authHeader, idUser) },
            defaultErrorMessage = "Delete Follow Failure"
        )
    }

    suspend fun getReviews(authHeader: String?, idUser: Int?): ApiResponse<List<Reviews>> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getReviews(authHeader, idUser) },
            defaultErrorMessage = "Get Reviews Failure"
        )
    }

    suspend fun postReviews(authHeader: String?,  fields: HashMap<String?, Any?>): ApiResponse<Message> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postReview(authHeader, fields) },
            defaultErrorMessage = "Post Reviews Failure"
        )
    }

    suspend fun getFacebookData(userId: String, authHeader: String):
            ApiResponse<FacebookResponse?> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.getFacebookData("https://graph.facebook.com/v11.0/$userId",
                "Bearer $authHeader", "id,first_name,last_name,email,picture", "normal") },
            defaultErrorMessage = "Get Facebook Data Failure"
        )
    }

    suspend fun postUploadMedia(authHeader: String?, image: MultipartBody.Part, key: RequestBody): ApiResponse<MessagesUpload> {
        val apiInterface = retrofit.create(ApiService::class.java)
        return getResponse(
            request = { apiInterface.postUploadMedia(authHeader, image, key) },
            defaultErrorMessage = "Post Upload Media Failure"
        )
    }

    private suspend fun <T> getResponse(
        request: suspend () -> Response<T>,
        defaultErrorMessage: String
    ): ApiResponse<T> {
        return try {
            val result = request.invoke()
            if (result.isSuccessful) {
                return ApiResponse.success(result.body())
            } else if (result.code() == 500) {
                ApiResponse.error(result.code().toString(), null)
            } else {
                try {
                    val message: ErrorResponse = Gson().fromJson(
                        result.errorBody()?.charStream(),
                        ErrorResponse::class.java
                    )
                    ApiResponse.error( message.error ?: message.message ?: defaultErrorMessage, null)
                } catch (e: Exception) {
                    Log.d("errorparse", result.code().toString())
                    val errorResponse = ErrorUtils.parseError(result, retrofit)
                    ApiResponse.error(errorResponse?.message ?: defaultErrorMessage, errorResponse)
                }
            }
        } catch (se3: SocketTimeoutException) {
            ApiResponse.error("timeout", null)
        } catch (e: Throwable) {
            Log.d("exceptionss", e.toString())
            ApiResponse.error("Error internet conection", null)
        }
    }
}