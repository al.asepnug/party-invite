package com.muxe.partyinvite.data.entity

import com.google.gson.annotations.SerializedName

data class Login(

	@field:SerializedName("token")
	val token: String? = null
)
