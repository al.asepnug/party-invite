package com.muxe.partyinvite.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.muxe.partyinvite.data.entity.User
import com.muxe.partyinvite.utils.ConverterAvatar
import com.muxe.partyinvite.utils.ConverterCover
import com.muxe.partyinvite.utils.ConvertersPayment

@Database(entities = [User::class], version = 1, exportSchema = false)
@TypeConverters(ConverterAvatar::class, ConverterCover::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun databaseDao(): DatabaseDao
}