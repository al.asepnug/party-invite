package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Party(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("owner")
    val owner: Owner? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("participants_count")
    val participantsCount: Int? = null,

    @field:SerializedName("is_favorite")
    val isFavorite: Boolean? = null,

    @field:SerializedName("is_joined")
    val isJoined: Boolean? = null,

    @field:SerializedName("distance")
    val distance: Double? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("ticket_count")
    val ticketCount: Int? = null,

    @field:SerializedName("latitude")
    val latitude: Double? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("avatar")
    val avatar: Avatar? = null,

    @field:SerializedName("cover")
    val cover: Cover? = null,

    @field:SerializedName("postalcode")
    val postalcode: String? = null,

    @field:SerializedName("price")
    val price: Double? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("favorited_count")
    val favoritedCount: Int? = null,

    @field:SerializedName("longitude")
    val longitude: Double? = null,

    @field:SerializedName("participants")
    val participants: List<Owner>? = null,

    @field:SerializedName("favorited")
    val favorited: List<Owner>? = null,

    @field:SerializedName("type_id")
    val typeId: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("type_music")
    val typeMusic: String? = null,

    @field:SerializedName("type_music_id")
    val typeMusicId: Int? = null,

    @field:SerializedName("share_url")
    val shareUrl: String? = null
) : Parcelable
