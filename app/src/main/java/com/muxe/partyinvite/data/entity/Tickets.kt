package com.muxe.partyinvite.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tickets(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("qr")
	val qr: String? = null,

	@field:SerializedName("code")
	val code: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("user")
	val user: Owner? = null,

	@field:SerializedName("party")
	val party: Party? = null
) : Parcelable

